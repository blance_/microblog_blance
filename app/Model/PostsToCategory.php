<?php
App::uses('AppModel', 'Model');

class PostsToCategory extends AppModel {
	
	public $validate = array(
		'post_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'category_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);

	public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
		),
		'Post' => array(
			'className' => 'Post',
			'foreignKey' => 'post_id',
		),
	);
}
