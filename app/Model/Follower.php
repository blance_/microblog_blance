<?php
App::uses('AppModel', 'Model');

class Follower extends AppModel {

	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'follower_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'created_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'deleted_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
	);
}
