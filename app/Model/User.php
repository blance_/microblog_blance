<?php
App::uses('AppModel', 'Model');

class User extends AppModel {

	public $validate = array(
		'firstname' => array(
			'between' => array(
				'rule' => array('lengthBetween', 2, 32),
				'message' => 'Firstname should be at least 2 chars long',
				'required' => true,
			),
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'lastname' => array(
			'between' => array(
				'rule' => array('lengthBetween', 2, 32),
				'message' => 'Firstname should be at least 2 chars long',
				'required' => true,
			),
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'username' => array(
			'between' => array(
				'rule' => array('lengthBetween', 2, 32),
				'message' => 'Firstname should be at least 2 chars long',
				'required' => true,
			),
			'notBlank' => array(
				'rule' => array('notBlank'),
				'required' => true,
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This username has already been taken',
			),
		),
		'password' => array(
			'alphaNumeric' => array(
				'rule' => array('alphaNumeric'),
				'required' => true,
			),
			'between' => array(
				'rule' => array('lengthBetween', 2, 40),
				'message' => 'Password should be at least 2 chars long',
				'required' => true,
			),
			'notBlank' => array(
				'rule' => array('notBlank'),
				'required' => true,
			),
			'confirmpassword'=>array(
				'rule'=>array('confirmpassword'),
				'message'=>'Password must match Password Confirmation',
			),
		),
		'confirmpassword' => array(
			'alphaNumeric' => array(
				'rule' => array('alphaNumeric'),
				'required' => true,
			),
			'between' => array(
				'rule' => array('lengthBetween', 2, 40),
				'message' => 'Password should be at least 2 chars long',
				'required' => true,
			),
			'notBlank' => array(
				'rule' => array('notBlank'),
				'required' => true,
			),
			'confirmpassword'=>array(
				'rule'=>array('confirmpassword'),
				'message'=>'Password Confirmation must match Password',
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Input a correct email',
			),
			'notBlank'=>array(
				'rule'=>array('notBlank'),
				'required'=>true,
			),
			'unique'=>array(
				'rule'=>'isUnique',
				'message'=>'This email has already been taken',
			),
		),
		'activate_token' => array(
			'maxLength' => array(
				'rule' => array('maxLength', '64'),
			),
		),
		'email_status' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
		),
		'verified' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
		),
		'privilege' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
		),
		'active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
		),
		'created_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'modified_at' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
	);

	public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Post' => array(
			'className' => 'Post',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

	public function confirmpassword() {
		if($this->data['User']['password'] != $this->data['User']['confirmpassword']) {
			return false;
		} else {
			return true;
		}
	}

	public function beforeSave($options = array()) {
		if(!empty($this->data['User']['password']) && !empty($this->data['User']['confirmpassword'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
			$this->data['User']['confirmpassword'] = AuthComponent::password($this->data['User']['confirmpassword']);
		}
		return true;
	}
}
