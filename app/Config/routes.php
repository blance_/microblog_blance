<?php
		
	/**[GUEST / USER / ADMIN] Routes to view public posts*/
	Router::connect('/', ['controller' => 'posts', 'action' => 'index']);
	
	/**[GUEST / USER / ADMIN] Routes to view public posts*/
	Router::connect('/:title-:id', 
		['controller' => 'posts', 'action' => 'view'],
		[
			'pass' => ['title', 'id'],
			'id' => '[0-9]+'
		]
	);

	/**[ADMIN] Login */
	Router::connect('/admin', ['controller' => 'users', 'action' => 'signin']);

	/**[ADMIN] Posts UI */
	Router::connect('/posts', ['controller' => 'posts', 'action' => 'show']);
	
	/**[ADMIN] Users UI*/
	Router::connect('/users', ['controller' => 'users', 'action' => 'index']);
	
	/**[ADMIN] Timeline UI*/
	Router::connect('/timeline', ['controller' => 'posts', 'action' => 'timeline']);

	/**[ADMIN] Profile UI*/
	Router::connect('/user-profile/:username', 
		['controller' => 'users', 'action' => 'profile'],
		[
			'pass' => ['username'],
	]);

	/**[ADMIN] Edit Profile*/
	Router::connect('/edit-profile/:username', 
		['controller' => 'users', 'action' => 'edit'],
		[
			'pass' => ['username'],
	]);

	/**[ADMIN] Edit Post UI*/
	Router::connect('/edit-post/:id', 
		['controller' => 'posts', 'action' => 'edit'],
		[
			'pass' => ['id'],
			'id' => '[0-9]+'
	]);

	/**[ADMIN] Timeline UI*/
	Router::connect('/create-post', ['controller' => 'posts', 'action' => 'add']);

	/**[ADMIN] Add Users*/
	Router::connect('/add-user', ['controller' => 'users', 'action' => 'newMember']);

	/** Edit Comment*/
	Router::connect('/edit-comment/:id', 
		['controller' => 'posts', 'action' => 'editComment'],
		[
			'pass' => ['id'],
			'id' => '[0-9]+'
	]);

	/** Browse Categories */
	Router::connect('/browse-category/:id', 
		['controller' => 'posts', 'action' => 'category'],
		[
			'pass' => ['id'],
			'id' => '[0-9]+'
	]);

	/** Show Followers */
	Router::connect('/followers/:id', 
		['controller' => 'followers', 'action' => 'showFollowers'],
		[
			'pass' => ['id'],
			'id' => '[0-9]+'
	]);

	/** Show Following */
	Router::connect('/following/:id', 
	['controller' => 'followers', 'action' => 'showFollowing'],
	[
		'pass' => ['id'],
		'id' => '[0-9]+'
	]);

	/**[ADMIN] Categories*/
	Router::connect('/categories', ['controller' => 'categories', 'action' => 'categories']);

	/**[ADMIN] Logout*/
	Router::connect('/exit', ['controller' => 'users', 'action' => 'logout']);

	/**[USER] Registration */
	Router::connect('/register', ['controller' => 'users', 'action' => 'register']);

	/**[USER] Login */
	Router::connect('/login', ['controller' => 'users', 'action' => 'login']);

	/**[USER] Logout */
	Router::connect('/logout', ['controller' => 'users', 'action' => 'userLogout']);
	
	/**[USER] Routes to view profile page */
	Router::connect('/my-profile/:username', 
		['controller' => 'users', 'action' => 'profileUser'],
		[
			'pass' => ['username']
	]);

	/**[USER] Routes to view post comments */
	Router::connect('/view-comments/:id', 
		['controller' => 'posts', 'action' => 'viewComments'],
		[
			'pass' => ['id'],
			'id' => '[0-9]+'
	]);

	/**Forgot Password */
	Router::connect('/forgot-password', ['controller' => 'users', 'action' => 'forgotPassword']);

	/**Reset Password */
	Router::connect('/reset-password', ['controller' => 'users', 'action' => 'resetPassword']);

	/**Change Password */
	Router::connect('/change-password', ['controller' => 'users', 'action' => 'changePassword']);

	CakePlugin::routes();
	require CAKE . 'Config' . DS . 'routes.php';
