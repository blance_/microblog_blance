<?php
/**
 * User Fixture
 */
class UserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'firstname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'lastname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 32, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'confirmpassword' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'email' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'email_status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1 for activated, 0 for not yet activated '),
		'profile_pic' => array('type' => 'string', 'null' => false, 'default' => 'default.png', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_at' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified_at' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'activate_token' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'active' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1 for active, 0 for not active'),
		'role' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1 for admin, 0 for users'),
		'verified' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1 for verified, 0 for not verified'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '1',
			'firstname' => 'blance',
			'lastname' => 'sanchez',
			'username' => 'blance',
			'password' => 'fd6bb8c0aecb151c3a6838641ecfc1abd1ae674b',
			'confirmpassword' => 'fd6bb8c0aecb151c3a6838641ecfc1abd1ae674b',
			'email' => 'blancessanchez30@gmail.com1',
			'email_status' => 1,
			'profile_pic' => 'cover-3-default-16-9-lg.jpg',
			'created_at' => '2018-07-20 14:19:32',
			'modified_at' => '0000-00-00 00:00:00',
			'activate_token' => '',
			'active' => 1,
			'role' => 1,
			'verified' => 1
		),
		array(
			'id' => '2',
			'firstname' => '',
			'lastname' => 'urgot',
			'username' => 'username',
			'password' => 'fd6bb8c0aecb151c3a6838641ecfc1abd1ae674b',
			'confirmpassword' => 'fd6bb8c0aecb151c3a6838641ecfc1abd1ae674b',
			'email' => 'email@',
			'email_status' => 1,
			'profile_pic' => 'default.png',
			'created_at' => '2018-07-24 11:31:31',
			'modified_at' => '0000-00-00 00:00:00',
			'activate_token' => '',
			'active' => 1,
			'role' => 0,
			'verified' => 1
		),
	);

}
