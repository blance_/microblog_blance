<?php
App::uses('User', 'Model');

/**
 * User Test Case
 */
class UserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user',
		'app.comment',
		'app.post',
		'app.like',
		'app.repost',
		'app.category',
		'app.posts_to_category',
		'app.password_token'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->User = ClassRegistry::init('User');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->User);

		parent::tearDown();
	}

/**
 * testConfirmpassword method
 *
 * @return void
 */
	public function testConfirmpassword() {
		$this->markTestIncomplete('testConfirmpassword not implemented.');
	}

}
