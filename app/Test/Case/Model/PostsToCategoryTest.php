<?php
App::uses('PostsToCategory', 'Model');

/**
 * PostsToCategory Test Case
 */
class PostsToCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.posts_to_category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PostsToCategory = ClassRegistry::init('PostsToCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PostsToCategory);

		parent::tearDown();
	}

}
