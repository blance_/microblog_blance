<?php
App::uses('Post', 'Model');

/**
 * Post Test Case
 */
class PostTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.post',
		'app.user',
		'app.comment',
		'app.follower',
		'app.login_token',
		'app.password_token',
		'app.repost',
		'app.like'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Post = ClassRegistry::init('Post');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Post);

		parent::tearDown();
	}

	// public $post = [
	// 	[
	// 		'id' => 1,
	// 		'username' => 'john.doe',
	// 		'password' => 'loremipsum',
	// 		'role' => 'admin',
	// 		'created' => '2015-08-08 20:51:50',
	// 		'modified' => '2015-08-08 20:51:50'
	// 	],
	// ];

}
