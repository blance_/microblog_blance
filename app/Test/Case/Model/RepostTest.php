<?php
App::uses('Repost', 'Model');

/**
 * Repost Test Case
 */
class RepostTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.repost',
		'app.post',
		'app.user',
		'app.comment',
		'app.follower',
		'app.login_token',
		'app.password_token',
		'app.like'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Repost = ClassRegistry::init('Repost');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Repost);

		parent::tearDown();
	}

}
