<?php
App::uses('LikesController', 'Controller');

/**
 * LikesController Test Case
 */
class LikesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.like',
		'app.post',
		'app.user',
		'app.comment',
		'app.password_token',
		'app.repost',
		'app.category',
		'app.posts_to_category'
	);

/**
 * testLike method
 *
 * @return void
 */
	public function testLike() {
		$this->markTestIncomplete('testLike not implemented.');
	}

/**
 * testUnlike method
 *
 * @return void
 */
	public function testUnlike() {
		$this->markTestIncomplete('testUnlike not implemented.');
	}

}
