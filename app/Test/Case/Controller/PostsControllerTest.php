<?php
App::uses('PostsController', 'Controller');

/**
 * PostsController Test Case
 */
class PostsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.post',
		'app.user',
		'app.comment',
		'app.password_token',
		'app.repost',
		'app.like',
		'app.category',
		'app.posts_to_category',
		'app.follower'
	);

/**
 * testShow method
 *
 * @return void
 */
	public function testShow() {
		$this->markTestIncomplete('testShow not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testEditComment method
 *
 * @return void
 */
	public function testEditComment() {
		$this->markTestIncomplete('testEditComment not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testDeleteComment method
 *
 * @return void
 */
	public function testDeleteComment() {
		$this->markTestIncomplete('testDeleteComment not implemented.');
	}

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testPublish method
 *
 * @return void
 */
	public function testPublish() {
		$this->markTestIncomplete('testPublish not implemented.');
	}

/**
 * testUnpublish method
 *
 * @return void
 */
	public function testUnpublish() {
		$this->markTestIncomplete('testUnpublish not implemented.');
	}

/**
 * testReset method
 *
 * @return void
 */
	public function testReset() {
		$this->markTestIncomplete('testReset not implemented.');
	}

/**
 * testViewComments method
 *
 * @return void
 */
	public function testViewComments() {
		$this->markTestIncomplete('testViewComments not implemented.');
	}

/**
 * testApprove method
 *
 * @return void
 */
	public function testApprove() {
		$this->markTestIncomplete('testApprove not implemented.');
	}

/**
 * testHide method
 *
 * @return void
 */
	public function testHide() {
		$this->markTestIncomplete('testHide not implemented.');
	}

/**
 * testCategory method
 *
 * @return void
 */
	public function testCategory() {
		$this->markTestIncomplete('testCategory not implemented.');
	}

/**
 * testRepost method
 *
 * @return void
 */
	public function testRepost() {
		$this->markTestIncomplete('testRepost not implemented.');
	}

/**
 * testUnrepost method
 *
 * @return void
 */
	public function testUnrepost() {
		$this->markTestIncomplete('testUnrepost not implemented.');
	}

/**
 * testTimeline method
 *
 * @return void
 */
	public function testTimeline() {
		$this->markTestIncomplete('testTimeline not implemented.');
	}

}
