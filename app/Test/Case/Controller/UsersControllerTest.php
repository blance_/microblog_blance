<?php
App::uses('UsersController', 'Controller');

/**
 * UsersController Test Case
 */
class UsersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user',
		'app.comment',
		'app.post',
		'app.like',
		'app.repost',
		'app.category',
		'app.posts_to_category',
		'app.password_token',
		'app.follower'
	);
	
	//public $dropTables = false; 
/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testNewMember method
 *
 * @return void
 */
	public function testNewMember() {
		$this->markTestIncomplete('testNewMember not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testLogin method
 *
 * @return void
 */
	public function testLogin() {
		$this->markTestIncomplete('testLogin not implemented.');
	}

/**
 * testSignin method
 *
 * @return void
 */
	public function testSignin() {
		$this->markTestIncomplete('testSignin not implemented.');
	}

/**
 * testLogout method
 *
 * @return void
 */
	public function testLogout() {
		$this->markTestIncomplete('testLogout not implemented.');
	}

/**
 * testUserLogout method
 *
 * @return void
 */
	public function testUserLogout() {
		$this->markTestIncomplete('testUserLogout not implemented.');
	}

/**
 * testProfile method
 *
 * @return void
 */
	public function testProfile() {
		$this->markTestIncomplete('testProfile not implemented.');
	}

/**
 * testProfileUser method
 *
 * @return void
 */
	public function testProfileUser() {
		$this->markTestIncomplete('testProfileUser not implemented.');
	}

/**
 * testDeactivate method
 *
 * @return void
 */
	public function testDeactivate() {
		$this->markTestIncomplete('testDeactivate not implemented.');
	}

/**
 * testActivate method
 *
 * @return void
 */
	public function testActivate() {
		$this->markTestIncomplete('testActivate not implemented.');
	}

/**
 * testRegister method
 *
 * @return void
 */
	public function testRegister() {
		$result = $this->testAction('/users');
		debug($result);
	}

/**
 * testVerify method
 *
 * @return void
 */
	public function testVerify() {
		$this->markTestIncomplete('testVerify not implemented.');
	}

/**
 * testChangePassword method
 *
 * @return void
 */
	public function testChangePassword() {
		$this->markTestIncomplete('testChangePassword not implemented.');
	}

}
