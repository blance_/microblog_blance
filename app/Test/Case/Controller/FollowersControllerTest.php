<?php
App::uses('FollowersController', 'Controller');

/**
 * FollowersController Test Case
 */
class FollowersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.follower',
		'app.user',
		'app.comment',
		'app.post',
		'app.like',
		'app.repost',
		'app.category',
		'app.posts_to_category',
		'app.password_token'
	);

/**
 * testFollow method
 *
 * @return void
 */
	public function testFollow() {
		$this->markTestIncomplete('testFollow not implemented.');
	}

/**
 * testUnfollow method
 *
 * @return void
 */
	public function testUnfollow() {
		$this->markTestIncomplete('testUnfollow not implemented.');
	}

/**
 * testShowFollowing method
 *
 * @return void
 */
	public function testShowFollowing() {
		$this->markTestIncomplete('testShowFollowing not implemented.');
	}

/**
 * testShowFollowers method
 *
 * @return void
 */
	public function testShowFollowers() {
		$this->markTestIncomplete('testShowFollowers not implemented.');
	}

}
