<?php
App::uses('PasswordTokensController', 'Controller');

/**
 * PasswordTokensController Test Case
 */
class PasswordTokensControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.password_token',
		'app.user',
		'app.comment',
		'app.post',
		'app.like',
		'app.repost',
		'app.category',
		'app.posts_to_category',
		'app.follower'
	);

}
