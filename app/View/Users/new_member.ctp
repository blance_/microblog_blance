<?php echo $this->start('navigation'); ?>
		<li class="active"><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-users']) . 'Users', ['controller' => 'users', 'action' => 'index'], ['escape' => false]);?></li>
		<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil']) . 'Posts', ['controller' => 'posts', 'action' => 'show'], ['escape' => false]);?></li>
		<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-calendar']) . 'Timeline', ['controller' => 'posts', 'action' => 'timeline'], ['escape' => false]);?></li>
		<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-bar-chart']) . 'Categories', ['controller' => 'categories', 'action' => 'categories'], ['escape' => false]);?></li>
<?php $this->end(); ?>
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo __('Add New User'); ?></h3>
				</div>
				<div class="box-body">
				<?php echo $this->Form->create('User', ['enctype' => 'multipart/form-data', 'url' => ['action' => 'newMember']]); ?>
					<div class="form-group">
						<?php echo $this->Form->input('firstname', ['class' => 'form-control', 'placeholder' => 'First Name', 'label' => false]);?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('lastname', ['class' => 'form-control', 'placeholder' => 'Last Name', 'label' => false]);?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('username', ['class' => 'form-control', 'placeholder' => 'Username', 'label' => false]);?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('email', ['class' => 'form-control', 'placeholder' => 'Email Address', 'label' => false]);?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('password', ['class' => 'form-control', 'placeholder' => 'Password', 'label' => false]);?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('confirmpassword', ['type' => 'password', 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'label'=>false]);?>
					</div>
				
					<div class="form-group">
						<?php echo $this->Form->input('role', ['options' => $role, 'empty'  => 'Select Role', 'type' => 'select', 'class' => 'form-control', 'label' => false]); ?>
					</div>

					<div class="box-footer">
						<?php echo $this->Form->submit('Register', ['class' => 'btn btn-success btn-block btn-flat']); ?>
					</div>
			</div>
		</div>
	</div>
</section>


