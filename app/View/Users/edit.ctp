<?php echo $this->start('navigation'); ?>
	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-users']) . 'Users', ['controller'=>'users', 'action'=>'index'], ['escape' => false]);?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil']) . 'Posts', ['controller'=>'posts', 'action'=>'show'], ['escape' => false]);?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-calendar']) . 'Timeline', ['controller'=>'posts', 'action'=>'timeline'], ['escape' => false]);?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-bar-chart']) . 'Categories', ['controller'=>'categories', 'action'=>'categories'], ['escape' => false]);?></li>
<?php $this->end(); ?>
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo __('Profile Settings'); ?></h3>
				</div>
				<div class="box-body">
				<?php echo $this->Form->create('User', ['enctype'=>'multipart/form-data']); ?>
					<div class="form-group">
						<?php echo $this->Form->input('firstname', ['class'=>'form-control']); ?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('lastname', ['class'=>'form-control']); ?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('username', ['class'=>'form-control']); ?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('email', ['class'=>'form-control']); ?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('profile_pic', ['type'=>'file', 'label' => 'Profile Picture']); ?>
					</div>

					<?php echo $this->Html->link('Change password', ['controller' => 'users', 'action' => 'changePassword']); ?>
					
					<br>
					<div class="box-footer">
						<?php echo $this->Form->submit('Save'); ?>
					</div>
			</div>
		</div>
	</div>
</section>


