<body class="hold-transition lockscreen">
<div class="lockscreen-wrapper">
  <div class="lockscreen-item">
    <label> Email Address </label>
        <?php 
            echo $this->Form->create('User');
            echo $this->Form->input('email', ['class' => 'form-control', 'label' => false]);
        ?>
      <small> Enter your email to receive the password reset </small> <br><br>
        <?php echo $this->Form->submit('Send Confirmation', ['class' => 'btn btn-primary']); ?>
  </div>
  <div class="text-center">
  <?php echo $this->Html->link('Or I already have an account', ['controller' => 'users', 'action' => 'login']); ?><br>
  <?php echo $this->Html->link('Admin login', ['controller' => 'users', 'action' => 'signin']); ?>
  </div>
</div>