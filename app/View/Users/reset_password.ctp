<body class="hold-transition lockscreen">
  <div class="lockscreen-wrapper">
    <div class="lockscreen-item">
      <?php echo $this->Form->create('User'); ?>
      <label> Password </label>
        <?php echo $this->Form->input('password', ['class' => 'form-control', 'label' => false]); ?>
      <label> Confirm Password </label>
        <?php echo $this->Form->input('confirmpassword', ['class' => 'form-control', 'type' => 'password', 'label' => false]); ?> <br>
      <?php echo $this->Form->submit('Save Password', ['class' => 'btn btn-success']); ?>
    </div>
  </div>