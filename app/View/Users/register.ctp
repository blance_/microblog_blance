<div class="row">
  <div class="col-twelve tab-full">
    <h3 class="add-bottom"><?php echo __('Create an account'); ?></h3>	
			<?php echo $this->Form->create('User'); ?>
			<div>
					<?php echo $this->Form->input('firstname', array('class'=>'full-width', 'placeholder'=>'First Name', 'label'=>false));?>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div>
					<?php echo $this->Form->input('lastname', array('class'=>'full-width', 'placeholder'=>'Last Name', 'label'=>false));?>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div>
					<?php echo $this->Form->input('username', array('class'=>'full-width', 'placeholder'=>'Username', 'label'=>false));?>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div>
					<?php echo $this->Form->input('email', array('class'=>'full-width', 'placeholder'=>'Email Address', 'label'=>false));?>
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>
			<div>
					<?php echo $this->Form->input('password', array('class'=>'full-width', 'placeholder'=>'Password', 'label'=>false));?>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div>
					<?php echo $this->Form->input('confirmpassword', array('type'=>'password', 'class'=>'full-width', 'placeholder'=>'Confirm Password', 'label'=>false));?>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div>
					<div>
						<?php echo $this->Form->submit('Register', array('class'=>'btn btn--primary full-width')); ?>
					</div>
					<div>
						<br>
						<?php echo $this->Html->link('I already have an account', array('controller'=>'users','action' => 'user')) ?>
					</div>
			</div>
	</div>
</div>

