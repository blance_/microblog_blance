<p class="login-box-msg"><?php echo __('Log in to start your session'); ?></p>
<?php echo $this->Form->create('User'); ?>
<div class="form-group has-feedback">
	<?php echo $this->Form->input('username', array('class'=>'form-control', 'placeholder'=>'Username', 'label'=>false));?>
	<span class="glyphicon glyphicon-user form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
	<?php echo $this->Form->input('password', array('class'=>'form-control', 'placeholder'=>'Password', 'label'=>false));?>
	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
	<div class="col-xs-4">
		<?php echo $this->Form->submit('Login', array('class'=>'btn btn-primary btn-block btn-flat')); ?>
	</div>
</div><br>
<?php echo $this->Html->link('Forgot Password', ['controller' => 'users', 'action' => 'forgotPassword']); ?>

