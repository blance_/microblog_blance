<div class="row">
    <?php if (AuthComponent::user('username') == $user['User']['username']) : ?>
    <div class="col-six tab-full">
        <h3 class="add-bottom"><?php echo __('Create Post'); ?></h3>
        <?php echo $this->Form->create('Post', ['enctype' => 'multipart/form-data']); ?>
            <div>
                <?php echo $this->Form->input('title', ['class' => 'full-width', 'placeholder' => 'Title', 'label' => false]);?>
            </div>
            <div>
                <?php echo $this->Form->input('body', ['class' => 'full-width', 'placeholder' => 'Body', 'label' => false]);?> 
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('Category.categories', [
                    'type'=>'text',
                    'label'=>__('Add Categories', true),
                    'class' => 'full-width'
                ]); ?>
            <small>Separate each tag with comma. Eg: family, sports, ice cream</small>
            </div>
            <div>
                <?php echo $this->Form->input('upload', ['type' => 'file']); ?>    
            </div>
        <?php echo $this->Form->submit('Post', ['controller' => 'users', 'action' => 'profileUser'], ['class'=>'btn btn--stroke full-width']); ?>
    </div>
    <?php endif; ?>
    <div class="col-six tab-full">
        <div class="s-content__author">
            <?php echo $this->Html->image(h($user['User']['profile_pic']), ['class'=>'profile-user-img img-responsive img-circle', 'alt'=>'User Image']); ?>
                <div class="s-content__author-about">
                    <h4 class="s-content__author-name">
                        <?php echo $this->Html->link(h($user['User']['username']), ['controller' => 'users', 'action' => 'profileUser', 'id' => $user['User']['id']]); ?></a>
                    </h4>
                    <?php 
                    if(AuthComponent::user('id') != $user['User']['id']) :
                        if(!$followed) : ?> 
                            <?= $this->Form->postlink('Follow', ['controller' => 'followers', 'action' => 'follow', $user['User']['username']], ['class'=>'btn btn-primary btn-block']); ?>
                        <?php else : ?>
                            <?= $this->Form->postlink('Unfollow', ['controller' => 'followers', 'action' => 'unfollow', $user['User']['username']], ['class'=>'btn btn-warning btn-block']); ?>
                    <?php endif;
                    endif; ?><br>
                        <b>Followers:</b> <?php echo $this->Html->link($follower, ['controller'=>'followers', 'action'=>'showFollowers', 'id' => $user['User']['id']])?> <br>
                        <b>Following:</b> <?php echo $this->Html->link($following, ['controller'=>'followers', 'action'=>'showFollowing', 'id' => $user['User']['id']])?>
                    <p><i>Bio here soon!</i></p>
                    <label>First Name:</label>dfafdsaf
                </div>
            <?php echo $this->Html->link('Profile Settings', ['controller'=>'users', 'action'=>'edit', 'username' => $user['User']['username']], ['class'=>'btn btn--stroke full-width'])?><br><br>
        </div>
    </div>
</div>
<div class="row masonry-wrap">
    <div class="masonry">
        <div class="grid-sizer"></div>
        <?php foreach ($posts as $post) : ?>
            <article class="masonry__brick entry format-standard">
                <?php if(!empty($post['Post']['post_pic'])) : ?>
                    <div class="entry__thumb">
                        <?=$this->Html->image(h($post['Post']['post_pic']), ['height'=>600, 'width'=>400, 'class'=>'entry__thumb-link'])?>
                    </div>
                <?php endif; ?>
                <div class="entry__text">
                    <div class="entry__header">
                        <div class="entry__date">
                            <?php $this->Time->format('F jS, Y h:i A', $post['Post']['created_at'])?>
                        </div>
                        <h1 class="entry__title"><?php echo $this->Html->link(h($post['Post']['title']), ['controller'=>'posts', 'action'=>'view', 'id' => $post['Post']['id'], 'title' => h($post['Post']['title'])]);?></h1>
                    </div>
                    <div class="entry__excerpt">
                        <p><?= h($post['Post']['body']) ?></p>
                    </div>
                </div>
            </article>
        <?php endforeach; ?>
    </div>
</div>
</div>
<div class="row">
    <div class="col-full">
        <nav class="pgn">
            <ul>
                <li><?php  $paginate = $this->Paginator;
                     echo $paginate->first('First', ['class'=>'pgn__num'])?></li>
                <?php if ($paginate->hasPrev()) : ?>
                    <li><?php echo $paginate->prev('<<')?></li>
                <?php endif; ?>
                <li><?php echo $paginate->numbers(['modulus'=>2, 'class'=>'pgn__num'])?></li>
                <?php if ($paginate->hasNext()) : ?>
                    <li><?php echo $paginate->next('>>')?></li>
                <?php endif; ?>
                <li><?php echo $paginate->last('Last', ['class'=>'pgn__num'])?></li>
            </ul>
        </nav>
    </div>
</div>
