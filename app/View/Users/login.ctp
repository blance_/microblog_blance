<div class="row">
    <div class="col-twelve tab-full">
        <h3 class="add-bottom"><?php echo __('Login'); ?></h3>
        <?php echo $this->Form->create('User'); ?>
            <div>
                <?php echo $this->Form->input('username', ['class' => 'full-width', 'placeholder' => 'Username', 'label' => false]);?>
            </div>
            <div>
                <?php echo $this->Form->input('password', ['class' => 'full-width', 'placeholder' => 'Password', 'label' => false]);?>
            </div>
        <?php echo $this->Form->submit('Login', ['class' => 'btn btn--primary full-width']); ?>
		<?php echo $this->Html->link('I am a new member', ['controller' => 'users','action' => 'register']); ?> <br>
        <?php echo $this->Html->link('Forgot password', ['controller' => 'users', 'action' => 'forgotPassword']); ?>
    </div>
</div>



