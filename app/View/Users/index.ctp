
<?php echo $this->start('navigation'); ?>
	<li class="active"><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-users']) . 'Users', ['controller' => 'users', 'action' => 'index'], ['escape' => false]);?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil']) . 'Posts', ['controller' => 'posts', 'action' => 'show'], ['escape' => false]);?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-calendar']) . 'Timeline', ['controller' => 'posts', 'action' => 'timeline'], ['escape' => false]);?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-bar-chart']) . 'Categories', ['controller' => 'categories', 'action' => 'categories'], ['escape' => false]);?></li>
<?php $this->end(); ?>

<section class="content">
	<?= $this->Html->link('Add New User', ['controller' => 'users','action' => 'newMember'], ['class' => 'btn btn-primary']) ?><br><br>
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><?php echo __('List of Users'); ?></h3>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped" id="example1">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Username</th>
						<th>Email Address</th>
						<th>Profile Picture</th>
						<th>Email Verification Status</th>
						<th>Status</th>
						<th>View</th>
						<th>Edit</th>
						<th>Deactivate</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $user) : ?>
					<tr>
						<td><?= h($user['User']['firstname']); ?></td>
						<td><?= h($user['User']['lastname']); ?></td>
						<td><?= h($user['User']['username']); ?></td>
						<td><?= h($user['User']['email']); ?></td>
						<td><?= $this->Html->image(h($user['User']['profile_pic']), ['height'=>50, 'width'=>50]);?></td>
						<td><?php if ($user['User']['email_status'] == 0) : echo 'Not yet verified'; else : echo 'Verified'; endif;?></td>
						<td><?php if ($user['User']['active'] == 0) : echo 'Deactivated'; else : echo 'Active'; endif;?></td>
						<td><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']), ['controller'=>'users', 'action'=>'profile', 'username' => h($user['User']['username'])], ['escape' => false, 'class'=>'btn btn-default', 'target' => '_blank']);?></td>
						<td><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil']), ['controller'=>'users', 'action'=>'edit', 'username' => h($user['User']['username'])], ['escape' => false, 'class'=>'btn btn-default']);?></td>
						<td><?php 
						if($user['User']['id'] != 1)  :
							if ($user['User']['active'] == 1) : 
								echo $this->Form->postlink('Deactivate', ['controller'=>'users', 'action'=>'deactivate', $user['User']['id']], ['confirm' => 'Are you sure you want to deactivate this user?']); 
							else : 
								echo $this->Form->postlink('Activate', ['controller'=>'users', 'action'=>'activate', $user['User']['id']], ['confirm' => 'Are you sure you want to activate this user?']); 
							endif;
						endif;
						?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				<?php unset($user); ?>
			</table>
		</div>
	</div>
</section>
