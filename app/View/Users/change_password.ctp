
<?php echo $this->start('navigation'); ?>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-users')) . 'Users', array('controller'=>'users', 'action'=>'index'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')) . 'Posts', array('controller'=>'posts', 'action'=>'show'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-calendar')) . 'Timeline', array('controller'=>'posts', 'action'=>'timeline'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')) . 'Categories', array('controller'=>'categories', 'action'=>'categories'), array('escape' => false));?></li>
<?php $this->end(); ?>
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo __('Change Password'); ?></h3>
				</div>
				<div class="box-body">
				<?php echo $this->Form->create('User'); ?>
                <label> Old Password </label>
                    <div class="form-group">
						<?php echo $this->Form->input('oldpassword', array('class'=>'form-control', 'type' => 'password', 'label' => false)); ?>
					</div>
                <label> New Password </label>
					<div class="form-group">
						<?php echo $this->Form->input('password', array('class'=>'form-control', 'label' => false)); ?>
					</div>
                <label> Confirm New Password </label>
					<div class="form-group">
						<?php echo $this->Form->input('confirmpassword', array('class'=>'form-control', 'type' => 'password', 'label' => false)); ?>
					</div>

					<div class="box-footer">
						<?php echo $this->Form->submit('Post', array('class'=>'btn btn-primary')); ?>
					</div>
			</div>
		</div>
	</div>
</section>

