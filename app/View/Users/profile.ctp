<?php echo $this->start('navigation'); ?>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-users')) . 'Users', array('controller'=>'users', 'action'=>'index'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')) . 'Posts', array('controller'=>'posts', 'action'=>'show'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-calendar')) . 'Timeline', array('controller'=>'posts', 'action'=>'timeline'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')) . 'Categories', array('controller'=>'categories', 'action'=>'categories'), array('escape' => false));?></li>
<?php $this->end(); ?>

<section class="content">
    <?php echo $this->Html->link('Profile Settings', array('controller' => 'users', 'action' => 'edit', 'username' => $user['User']['username']), array('class'=>'btn btn-primary'))?><br><br>
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <?php echo $this->Html->image(h($user['User']['profile_pic']), array('class'=>'profile-user-img img-responsive img-circle', 'alt'=>'User Image')); ?>
                    <h3 class="profile-username text-center"><?= h($user['User']['firstname']).' '.h($user['User']['lastname'])?></h3>
                    <p class="text-muted text-center">
                        <?='@'. h($user['User']['username']). '&nbsp';
                            if ($user['User']['verified'] == 1) {
                            echo $this->Html->image('verified.png', array('alt'=>'Verified User', 'height'=>20, 'width'=>20, 'title'=>'Verified')); 
                        }
                        ?>
                    </p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Followers</b> <?php echo $this->Html->link($follower, array('controller'=>'followers', 'action'=>'showFollowers', 'id' => $user['User']['id']))?>
                        </li>
                        <li class="list-group-item">
                            <b>Following</b> <?php echo $this->Html->link($following, array('controller'=>'followers', 'action'=>'showFollowing', 'id' => $user['User']['id']))?>
                        </li>
                    </ul>
                    <?php 
                    if(AuthComponent::user('id') != $user['User']['id']) :
                        if(!$followed) : ?> 
                            <?= $this->Form->postlink('Follow', array('controller' => 'followers', 'action' => 'follow', $user['User']['username']), array('class'=>'btn btn-primary btn-block')); ?>
                        <?php else : ?>
                            <?= $this->Form->postlink('Unfollow', array('controller' => 'followers', 'action' => 'unfollow', $user['User']['username']), array('class'=>'btn btn-warning btn-block')); ?>
                    <?php endif;
                    endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Posts</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <?php if(!empty($posts)) :
                        foreach($posts as $post) : ?>
                        <div class="post">
                            <div class="user-block">
                            <?php if(AuthComponent::user('id') == $post['Post']['user_id']) : ?>
                                <li style='list-style-type: none;' class='dropdown pull-right'>
                                    <a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-bars'></i> <span class='caret'></span></a>
                                    <ul class='dropdown-menu' role='menu'>
                                        <li><?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'edit', 'id' => $post['Post']['id']), array('class'=>'btn btn-link'));?></li>
                                        <li><?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'delete', $post['Post']['id']), array('confirm'=>'Are you sure you want to delete this post?','class'=>'btn btn-link'));?></li>
                                    </ul>
                                </li><input type='hidden' id='uid' class='form-control'>
                            <?php endif; ?>
                            <?php echo $this->Html->image($user['User']['profile_pic'], array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); ?>
                                <span class="username">
                                <?= $this->HTML->link(h($post['User']['username']), array('controller'=>'users', 'action'=>'profile', 'username' => h($post['User']['username'])), array('class'=>'btn btn-link'));?>
                                </span>
                            <span class="description"><?php echo $post['Post']['created_at']?></span>
                            </div>
                            <h4><b><?php echo $post['Post']['title'] ?></b></h4>
                            <p> <?php 
                                $text = explode(" ", h($post['Post']['body']));
                                $newstring = "";
                                foreach ($text as $word) {
                                    if (substr($word, 0, 1) == "@") {
                                    echo $newstring .= "<a href='http://cakeblog.com/user-profile/".substr($word, 1)."'>".h($word). "</a>";
                                    } else {
                                    echo $newstring .= h($word)." ";
                                    }
                                }
                            ?></p>
                            <?php if(!empty($post['Post']['post_pic'])) : ?>
                                <?=$this->Html->image(h($post['Post']['post_pic']), array('height' => 300, 'width' => 300))?>
                            <?php endif;?>
                        </div>
                        <?php foreach ($post['Comment'] as $comment) : ?>
                            <div class='post clearfix' id="commentStatus">
                                <div class='user-block'>
                                <?php if(AuthComponent::user('id') == $post['Post']['user_id']) : ?>
                                    <li style='list-style-type: none;' class='dropdown pull-right'>
                                        <a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-bars'></i> <span class='caret'></span></a>
                                        <ul class='dropdown-menu' role='menu'>
                                            <li><?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'editComment', $comment['id']), array('class'=>'btn btn-link'));?></li>
                                            <li><?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'deleteComment', $comment['id']), array('confirm'=>'Are you sure you want to delete this comment?', 'class'=>'btn btn-link'));?></li>
                                        </ul>
                                    </li><input type='hidden' id='uid' class='form-control'>
                                <?php endif; ?>
                                    <?php 
                                        echo $this->Html->image(h($post['User']['profile_pic']), array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); 
                                    ?>
                                    <span class='username'><a href=''><?php echo h($post['User']['username'])?></a></span>
                                    <span class='description'><?php echo h($comment['created_at'])?></span>
                                </div>
                                    <p data-target='combody'><?php echo h($comment['body'])?></p>
                            </div>
                        <?php endforeach; ?>
                            <?php   
                                echo $this->Form->create('Comment');
                                echo $this->Form->input('Comment.body', array('label'=>false, 'class'=>'form-control input-sm', 'type'=>'text'));
                                echo $this->Form->input('Comment.post_id', array('type'=>'hidden', 'value'=>$post['Post']['id']));
                                echo $this->Form->end('Comment');
                            ?>
                    <?php endforeach; else :?>
                        <p> No posts to show</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
