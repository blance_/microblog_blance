<body class="hold-transition lockscreen">
<div class="lockscreen-wrapper">
  <div class="lockscreen-item">
      <h4>Forgot Password</h4>
    <?php echo $this->Form->create('PasswordToken'); ?>
    <div class="form-group has-feedback">
        <?php echo $this->Form->input('email', array('class'=>'form-control', 'placeholder'=>'Email Address', 'label'=>false));?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <?php echo $this->Form->end('Send', array('controller' => 'passwordtokens', 'action'=>'forgot'), array('class'=>'btn btn-primary'))?>
  </div>
  <div class="help-block text-center">
    Enter your email to retrieve your account.
  </div>
  <div class="text-center">
      <?php echo $this->Html->link('Or sign in as a different user', array('controller' => 'users', 'action' => 'login')) ?>
  </div>
</div>