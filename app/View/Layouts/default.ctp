<!DOCTYPE html>
<html>
  <title><?php echo __($this->fetch('title')); ?></title>
<head>
	<?php echo $this->element('header'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="" class="logo">
      <span class="logo-mini"><b>M</b>B</span>
      <span class="logo-lg"><b>MicroBlog</b>Admin</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php
                echo $this->Html->image($currentUser['User']['profile_pic'], array('class'=>'user-image', 'alt'=>'User Image')); 
              ?>
              <span class="hidden-xs"><?= h($currentUser['User']['firstname']) . ' ' . h($currentUser['User']['lastname'])?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
			  	    <?= $this->Html->image($currentUser['User']['profile_pic'], array('class'=>'img-circle', 'alt'=>'User Image')); ?>
                <p>
                  <?= h($currentUser['User']['username']); ?>
                  <small>Member since <?= $this->Time->format('F jS, Y h:i A', $currentUser['User']['created_at'])?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <?= $this->Html->link('Profile', array('controller'=>'users', 'action' => 'profile', 'username'=> AuthComponent::user('username')), array('class'=>'btn btn-default btn-flat')) ?>
                </div>
                <div class="pull-right">		
					        <?= $this->Html->link('Log Out', array('controller'=>'users', 'action' => 'logout'), array('class'=>'btn btn-default btn-flat')) ?><br>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
			<?php echo $this->Html->image($currentUser['User']['profile_pic'], array('class'=>'img-circle', 'alt'=>'User Image')); ?>
        </div>
        <div class="pull-left info">
          <p><?= h($currentUser['User']['firstname']) . ' ' . h($currentUser['User']['lastname'])?></p>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
      <?php echo $this->fetch('navigation'); ?>
      </ul>
    </section>
  </aside>
  <div class="content-wrapper">
  		<?php echo $this->Flash->render(); ?>
		<?php echo $this->fetch('content'); ?>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 3.0
    </div>
    <strong>Copyright &copy; 2018 Blances Sanchez.</strong> All rights
    reserved.
  </footer>
</div>
<?php echo $this->element('footer'); ?>
</body>
</html>
