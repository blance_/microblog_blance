<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo __('Microblog | ' . $this->fetch('title')); ?></title>
<?php 
    echo $this->Html->meta('icon');
    echo $this->Html->css(array('AdminLTE.min.css', 'skins/_all-skins.min.css', 
    'bower_components/bootstrap/dist/css/bootstrap.min.css', 
    'bower_components/font-awesome/css/font-awesome.min.css', 
    'bower_components/Ionicons/css/ionicons.min.css', 
    'bower_components/morris.js/morris.css', 
    'bower_components/jvectormap/jquery-jvectormap.css', 
    'bower_components/jvectormap/jquery-jvectormap.css', 
    'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', 
    'bower_components/bootstrap-daterangepicker/daterangepicker.css', 
    'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css', 
    'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'));
?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
   <b>Micro</b>Blog</a>
  </div>
  <div class="login-box-body">
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->fetch('content'); ?>
  </div>
</div>
<?php 
    echo $this->Html->script('bower_components/jquery/dist/jquery.min.js');
    echo $this->Html->script('bower_components/bootstrap/dist/js/bootstrap.min.js');
?>
</body>
</html>
