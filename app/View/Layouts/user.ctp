
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo __($this->fetch('title')); ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php 
        echo $this->Html->css(array('base.css', 'vendor.css', 'main.css')); 
        echo $this->Html->meta('icon');
    ?>
</head>
<body id="top">
    <section class="s-pageheader">
        <header class="header">
            <div class="header__content row">
                <div class="header__logo">
                    <a class="logo" href="index.html">
                        <?= $this->Html->image('phi/logo.png', array('alt'=>'Homepage')); ?>
                    </a>
                </div>
                <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>
                <nav class="header__nav-wrap">
                    <h2 class="header__nav-heading h6">Site Navigation</h2>
                    <ul class="header__nav">
                        <li><?= $this->Html->link('Home', array('controller'=>'posts', 'action' => 'index')) ?></li> 
                        <li class="has-children">
                            <a href="#0" title="">Categories</a>
                            <ul class="sub-menu">
                                <?php foreach($categories as $category) : ?>
                                    <li><?= $this->Html->link(h($category['Category']['name']), array('controller' => 'posts', 'action' => 'category', 'id' => $category['Category']['id']));?></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <?php if(AuthComponent::user('id')) : ?>
                            <li><?= $this->Html->link('Profile', array('controller'=>'users', 'action' => 'profileUser', 'username' => AuthComponent::user('username'))) ?></li> 
                            <li><?= $this->Html->link('Logout', array('controller'=>'users', 'action' => 'userLogout')) ?></li> 
                        <?php else :?>
                            <li><?= $this->Html->link('Login', array('controller'=>'users', 'action' => 'login')) ?></li> 
                        <?php endif; ?>
                    </ul>
                    <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>
                </nav>
            </div>
        </header>
    </section>
    <section class="s-content">
        <?php echo $this->Flash->render(); ?>
		<?php echo $this->fetch('content'); ?>
    </section>
    <section class="s-extra">
        <div class="row top">
            <div class="col-eight md-six tab-full popular">
                <h3>Popular Posts</h3>
                <div class="block-1-2 block-m-full popular__posts">
                <?php foreach ($popular as $p) : ?>
                    <article class="col-block popular__post">
                        <a class="popular__thumb">
                            <?php if(!empty($p['Post']['post_pic'])) : 
                                echo $this->Html->image(h($p['Post']['post_pic']), array('height' => 150, 'width' => 150));
                            else :
                                echo $this->Html->image('post.png');
                            endif; ?>
                        </a>
                        <h5><?php echo $this->Html->link(h($p['Post']['title']), array('controller'=>'posts', 'action'=>'view', 'id' => $p['Post']['id'], 'title' => h($p['Post']['title'])))?></h5>
                        <section class="popular__meta">
                                <span class="popular__author"><span>By</span> <a href="#0"><?= h($p['User']['username'])?></a></span>
                            <span class="popular__date"><span>on </span><?= $this->Time->format('F jS, Y', $p['Post']['created_at'])?></span>
                        </section>
                    </article>
                <?php endforeach; ?>
                </div>
            </div>
            <div class="col-four md-six tab-full about">
                <h3>About MicroBlog</h3>
                <p>This is a blog, fortunately micro. A MicroBlog. Created by Blances Sanchez</p>
            </div>
        </div>
        <div class="row bottom tags-wrap">
            <div class="col-full tags">
                <h3>Categories</h3>
                <div class="tagcloud">
                    <?php foreach($categories as $category) : ?>
                        <?= $this->Html->link(h($category['Category']['name']), array('controller' => 'posts', 'action' => 'category', 'id' => h($category['Category']['id'])));?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <footer class="s-footer">
        <div class="s-footer__bottom">
            <div class="row">
                <div class="col-full">
                    <div class="s-footer__copyright">
                        <span>© Copyright MicroBlog 2018</span> 
                    </div>

                    <div class="go-top">
                        <a class="smoothscroll" title="Back to Top" href="#top"></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="preloader">
        <div id="loader">
            <div class="line-scale">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</body>
<?php echo $this->Html->script(array('modernizr.js', 'pace.min.js', 'jquery-3.2.1.min.js', 'plugins.js', 'main.js')); ?>
</html>