
<html>
  <title><?php echo $this->fetch('title'); ?></title>
<head>
	<?php echo $this->element('header'); ?>
</head>

<section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
</section>

<?php echo $this->element('footer'); ?>