
<?php echo $this->start('navigation'); ?>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-users')) . 'Users', array('controller'=>'users', 'action'=>'index'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')) . 'Posts', array('controller'=>'posts', 'action'=>'show'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-calendar')) . 'Timeline', array('controller'=>'posts', 'action'=>'timeline'), array('escape' => false));?></li>
    <li class="active"><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')) . 'Categories', array('controller'=>'categories', 'action'=>'categories'), array('escape' => false));?></li>
<?php $this->end(); ?>

<section class="content">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">List of Categories with Frequencies</h3>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped" id="example1">
				<thead>
					<tr>
						<th>Category Name</th>
						<th>Frequencies</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($cat as $c) : ?>
					<tr>
						<td><?= h($c['Category']['name']);?></td>
						<td><?= h($c['Category']['frequency']);?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				<?php unset($cat); ?>
			</table>
		</div>
	</div>
</section>
