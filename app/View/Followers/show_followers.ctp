<?php if (AuthComponent::user('id') != 1) : ?>
<div class="row">
    <div class="col-twelve">
        <h3><?php echo __('Followers of ' . $username); ?></h3>
        <div class="table-responsive">
            <table>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Profile Picture</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($followers as $f) : ?>
                        <tr>
                            <td><?= h($f['User']['username'])?></td>
                            <td><?= h($f['User']['firstname'])?></td>
                            <td><?= h($f['User']['lastname'])?></td>
                            <td><?php echo $this->Html->image(h($f['User']['profile_pic']), array('width'=>50, 'height'=>50))?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php else : ?>
<?php echo $this->start('navigation'); ?>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-users')) . 'Users', array('controller'=>'users', 'action'=>'index'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')) . 'Posts', array('controller'=>'posts', 'action'=>'show'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-calendar')) . 'Timeline', array('controller'=>'posts', 'action'=>'timeline'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')) . 'Categories', array('controller'=>'categories', 'action'=>'categories'), array('escape' => false));?></li>
<?php $this->end(); ?>
<section class="content">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><?php echo __('Followers of ' . $username); ?></h3>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped" id="example1">
				<thead>
					<tr>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Profile Picture</th>
					</tr>
				</thead>
				<tbody>
                    <?php foreach ($followers as $f) : ?>
					<tr>
                        <td><?= h($f['User']['username'])?></td>
                        <td><?= h($f['User']['firstname'])?></td>
                        <td><?= h($f['User']['lastname'])?></td>
                        <td><?php echo $this->Html->image(h($f['User']['profile_pic']), array('width'=>50, 'height'=>50))?></td>
					</tr>
                    <?php
                    endforeach; ?>
				</tbody>
				<?php unset($followers); ?>
			</table>
		</div>
	</div>
</section>
<?php endif; ?>

