<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible" id="<?php echo $key; ?>Message" class="<?php echo !empty($params['class']) ? $params['class'] : 'message'; ?>">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            <p><?php echo $message; ?></p>
        </div>
    </div>	
</div>