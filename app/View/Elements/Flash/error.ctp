<div class="row">
    <div class="alert-box alert-box--error hideit" id="<?php echo $key; ?>Message" class="<?php echo !empty($params['class']) ? $params['class'] : 'message'; ?>">
        <p><?php echo $message; ?></p>
        <i class="fa fa-times alert-box__close"></i>
    </div>
</div>

