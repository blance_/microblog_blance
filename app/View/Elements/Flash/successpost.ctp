<div class="row">
    <div class="alert-box alert-box--success hideit" id="<?php echo $key; ?>Message" class="<?php echo !empty($params['class']) ? $params['class'] : 'message'; ?>">
        <h4><i class="icon fa fa-check"></i> Success!</h4>    
        <p><?php echo $message; ?></p>
        <i class="fa fa-times alert-box__close"></i>
    </div>
</div>

