<div class="row">
    <div class="col-twelve">
        <h3><?php echo __('Comments in' . h($title)); ?></h3>
        <div class="table-responsive">
            <table>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Author</th>
                        <th>Email</th>
                        <th>Comment</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Approve</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($comments as $c) : ?>
                        <tr>
                            <td><?= h($c['User']['username']); ?></td>
                            <td><?= h($c['Comment']['author']); ?></td>
                            <td><?= h($c['Comment']['email']); ?></td>
                            <td><?= h($c['Comment']['body']); ?></td>
                            <td><?= $this->Time->format('F jS, Y h:i A', $c['Comment']['created_at'])?></td>
                            <td><?php if($c['Comment']['status'] == 0) : echo 'Hidden'; else : echo 'Published'; endif;?></td>
                            <td><?php if($c['Comment']['status'] == 0) : echo $this->Form->postlink('Approve', array('controller'=>'posts', 'action'=>'approve', $c['Comment']['id'], $id)); else : echo $this->Form->postlink('Hide', array('controller'=>'posts', 'action'=>'hide', $c['Comment']['id'], $id)); endif;?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-full">
        <nav class="pgn">
            <ul>
                <li><?php  $paginate = $this->Paginator;
                     echo $paginate->first('First', array('class'=>'pgn__num'))?></li>
                <?php if ($paginate->hasPrev()) : ?>
                    <li><?php echo $paginate->prev('<<')?></li>
                <?php endif; ?>
                <li><?php echo $paginate->numbers(array('modulus'=>2, 'class'=>'pgn__num'))?></li>
                <?php if ($paginate->hasNext()) : ?>
                    <li><?php echo $paginate->next('>>')?></li>
                <?php endif; ?>
                <li><?php echo $paginate->last('Last', array('class'=>'pgn__num'))?></li>
            </ul>
        </nav>
    </div>
</div>