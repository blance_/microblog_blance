
<?php echo $this->start('navigation'); ?>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-users')) . 'Users', array('controller'=>'users', 'action'=>'index'), array('escape' => false));?></li>
	<li class="active"><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')) . 'Posts', array('controller'=>'posts', 'action'=>'show'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-calendar')) . 'Timeline', array('controller'=>'posts', 'action'=>'timeline'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')) . 'Categories', array('controller'=>'categories', 'action'=>'categories'), array('escape' => false));?></li>
<?php $this->end(); ?>
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo __('Create Post'); ?></h3>
				</div>
				<div class="box-body">
				<?php echo $this->Form->create('Post', array('enctype'=>'multipart/form-data', 'url'=>array('action'=>'add'))); ?>
					<div class="form-group">
						<?php echo $this->Form->input('title', array('class'=>'form-control')); ?>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('body', array('class'=>'form-control')); ?>
					</div>
					<br>
					<div class="form-group">
						<?php echo $this->Form->input('Category.categories', array(
							'type'=>'text',
							'label'=>__('Add Categories', true),
							'class' => 'form-control'
						)); ?>
					<small>Separate each tag with comma. Eg: family, sports, ice cream</small>
					</div>

					<div class="form-group">
						<?php echo $this->Form->input('upload', array('type'=>'file')); ?>
					</div>

					<div class="box-footer">
						<?php echo $this->Form->submit('Post', array('class'=>'btn btn-primary')); ?>
					</div>
			</div>
		</div>
	</div>
</section>

