<?php echo $this->start('navigation'); ?>
    <li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-users')) . 'Users', array('controller'=>'users', 'action'=>'index'), array('escape' => false));?></li>
    <li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')) . 'Posts', array('controller'=>'posts', 'action'=>'show'), array('escape' => false));?></li>
    <li class="active"><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-calendar')) . 'Timeline', array('controller'=>'posts', 'action'=>'timeline'), array('escape' => false));?></li>
    <li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')) . 'Categories', array('controller'=>'categories', 'action'=>'categories'), array('escape' => false));?></li>
<?php $this->end(); ?>

<section class="content">
    <?php foreach($posts as $post) : ?>
        <div class="box">
            <div class="box default">
                    <div class="post">
                        <div class="user-block">
                            <?php if(AuthComponent::user('id') == $post['Post']['user_id']) : ?>
                                <li style='list-style-type: none;' class='dropdown pull-right'>
                                    <a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-bars'></i> <span class='caret'></span></a>
                                    <ul class='dropdown-menu' role='menu'>
                                        <li><?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'edit', 'id' => $post['Post']['id']), array('class'=>'btn btn-link'));?></li>
                                        <li><?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'delete', $post['Post']['id']), array('confirm'=>'Are you sure you want to delete this post?','class'=>'btn btn-link'));?></li>
                                    </ul>
                                </li><input type='hidden' id='uid' class='form-control'>
                            <?php else :?>
                                <li style='list-style-type: none;' class='dropdown pull-right'>
                                    <a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-bars'></i> <span class='caret'></span></a>
                                    <ul class='dropdown-menu' role='menu'>
                                        <li><?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'edit', 'id' => $post['Post']['id']), array('class'=>'btn btn-link'));?></li>
                                        <li><?= $this->Form->postlink('Unpublish', array('controller'=>'posts', 'action'=>'unpublish', $post['Post']['id']), array('confirm'=>'Are you sure you want to unpublish this post?','class'=>'btn btn-link'));?></li>
                                        <li><?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'delete', $post['Post']['id']), array('confirm'=>'Are you sure you want to delete this post?','class'=>'btn btn-link'));?></li>
                                    </ul>
                                </li><input type='hidden' id='uid' class='form-control'>
                            <?php endif; ?>
                                <?= $this->Html->image(h($post['User']['profile_pic']), array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); ?>
                                <span class="username">
                                    <?= $this->HTML->link(h($post['User']['username']), array('controller'=>'users', 'action'=>'profile', 'username' => $post['User']['username']), array('class'=>'btn btn-link'));?>
                                </span>
                            <span class="description"><?= $post['Post']['created_at']; ?></span>
                        </div>
                        <h4><b><?= h($post['Post']['title']); ?></b></h4>
                        <p><?php 
                                $text = explode(" ", h($post['Post']['body']));
                                $newstring = "";
                                foreach ($text as $word) {
                                    if (substr($word, 0, 1) == "@") {
                                    echo $newstring = "<a href='http://cakeblog.com/user-profile/".substr($word, 1)."'>".h($word). "</a>";
                                    } else {
                                    echo $newstring = h($word)." ";
                                    }
                                }
                            ?></p>
                        <?php if($post['Comment']): ?>
                        <hr>
                        <p> Comments </p>
                        <?php foreach ($post['Comment'] as $comment) : ?>
                            <div class='post clearfix' id=""> 
                                <div class='user-block'>
                                    <li style='list-style-type: none;' class='dropdown pull-right'>
                                        <a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-bars'></i> <span class='caret'></span></a>
                                        <ul class='dropdown-menu' role='menu'>
                                            <li><?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'editComment', 'id' => $comment['id']), array('class'=>'btn btn-link'));?></li>
                                            <li><?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'deleteComment', $comment['id']), array('confirm'=>'Are you sure you want to delete this comment?','class'=>'btn btn-link'));?></li>
                                        </ul>
                                    </li><input type='hidden' id='uid' class='form-control'>
                                    <?php 
                                        $image = h($post['User']['profile_pic']);
                                        echo $this->Html->image($image, array('class'=>'img-circle img-bordered-sm', 'alt'=>'User Image')); 
                                    ?>
                                    <span class='username'><?= $this->HTML->link(h($post['User']['username']), array('controller'=>'users', 'action'=>'profile', 'username' => h($post['User']['username'])), array('class'=>'btn btn-link'));?></a></span>
                                    <span class='description'><?= $comment['created_at']?></span>
                                </div>
                                    <p data-target='combody'><?= h($comment['body'])?></p>
                            </div>
                        <?php endforeach; ?>
                        <?php endif;?>
                        <?php   
                            echo $this->Form->create('Comment');
                            echo $this->Form->input('Comment.body', array('label'=>false, 'class'=>'form-control input-sm', 'type'=>'text'));
                            echo $this->Form->input('Comment.post_id', array('type'=>'hidden', 'value'=>$post['Post']['id']));
                            echo $this->Form->end('Comment');
                        ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?> 
</section>