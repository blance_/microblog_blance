<article class="row format-standard">
<div class="s-content__header col-full">
    <h1 class="s-content__header-title">
        <?php echo __(h($post['Post']['title'])); ?>
    </h1>
    <?php if(AuthComponent::user('id') == $post['Post']['user_id']) : ?>
        <?= $this->HTML->link('Edit', array('controller'=>'posts', 'action'=>'edit', 'id' => $post['Post']['id']));?>
        <?= $this->Form->postlink('Delete', array('controller'=>'posts', 'action'=>'delete', $post['Post']['id']), array('confirm'=>'Are you sure you want to delete this post?'));?>
    <?php endif; ?>
    <ul class="s-content__header-meta">
        <li class="date"><?= $this->Time->format('F jS, Y h:i A', $post['Post']['created_at'])?>
    </ul>
</div>
<div class="s-content__media col-full">
    <?php if(!empty(h($post['Post']['post_pic']))) : ?>
        <div class="s-content__post-thumb"><?=$this->Html->image($post['Post']['post_pic'], array('class'=>'center'))?></div>
    <?php endif; ?>
</div>
<div class="col-full s-content__main">
    <?php 
        $text = explode(" ", h($post['Post']['body']));
        $newstring = "";
        foreach ($text as $word) {
            if (substr($word, 0, 1) == "@") {
               echo $newstring = "<a href='http://cakeblog.com/my-profile/".substr($word, 1)."'>".h($word). "</a>";
            } else {
               echo $newstring = h($word)." ";
            }
		}
    ?>  
    <br>
    <?php 
    if ($authUser) : 
        if(!$reposted) : 
            echo $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-retweet')).'Repost', array('controller'=>'posts', 'action'=>'repost', $post['Post']['id'], AuthComponent::user('id')), array('escape' => false, 'class'=>'btn btn-default'));
        else :
            echo $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-retweet')).'Unrepost', array('controller'=>'posts', 'action'=>'unrepost', $post['Post']['id'], AuthComponent::user('id')), array('escape' => false, 'class'=>'btn btn-default'));
        endif;

        if($liked) : 
            echo $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-thumbs-o-down')).'Unlike', array('controller'=>'likes', 'action'=>'unlike', $post['Post']['id'], AuthComponent::user('id')), array('escape' => false, 'class'=>'btn btn-default'));
        else :
            echo $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-thumbs-o-up')).'Like', array('controller'=>'likes', 'action'=>'like', $post['Post']['id'], AuthComponent::user('id')), array('escape' => false, 'class'=>'btn btn-default'));
        endif; 
    endif; 
    ?>
    <br>
    <?php if (AuthComponent::user('id') == $post['User']['id']) : ?>
        <?= $this->Html->link('Manage comments', array('controller'=>'posts', 'action'=>'viewComments', 'id' => $post['Post']['id'])); ?>
    <?php endif; ?>
    <p class="s-content__tags">
        <span>Post Tags</span>
        <span class="s-content__tag-list">
            <?php foreach($tags as $tag) : ?>
                <?= $this->Html->link($tag['Category']['name'], array('controller' => 'posts', 'action' => 'category', 'id' => $tag['Category']['id']));?>
            <?php endforeach; ?>
        </span>
    </p>
    <div class="s-content__author">
    <?php echo $this->Html->image(h($post['User']['profile_pic']), array('class' => 'profile-user-img img-responsive img-circle', 'alt' => 'User Image')); ?>
        <div class="s-content__author-about">
            <h4 class="s-content__author-name">
                <?php echo $this->Html->link(h($post['User']['username']), array('controller'=>'users', 'action'=>'profileUser', 'username' => h($post['User']['username'])))?>
            </h4>
            <p><i>Bio here soon!</i></p>
        </div>
    </div>
</div>
</article>
<div class="comments-wrap">
<div id="comments" class="row">
    <div class="col-full">
        <h3 class="h2"><?= $com_count . ' comments'; ?></h3>
        <ol class="commentlist">
            <?php foreach ($comments as $comment) : 
                if ($comment['Comment']['status'] == 1) :
                    if ($comment['Comment']['user_id'] != 0) : ?>
                        <li class="depth-1 comment">
                            <div class="comment__avatar">
                                <?= $this->Html->image(h($comment['User']['profile_pic']), array('width'=>50, 'height'=>50, 'class'=>'avatar')); ?>
                            </div>
                            <div class="comment__content">
                                <div class="comment__info">
                                    <cite><?= h($comment['User']['username']); ?></cite>
                                    <div class="comment__meta">
                                        <time class="comment__time"><?= $this->Time->format('F jS, Y h:i A', $comment['Comment']['created_at'])?></time>
                                    </div>
                                </div>
                                <div class="comment__text">
                                <p><?= h($comment['Comment']['body'])?></p>
                                </div>
                            </div>
                        </li>
                    <?php else : ?>
                        <li class="depth-1 comment">
                            <div class="comment__avatar">
                                <?= $this->Html->image('guest.png', array('width'=>50, 'height'=>50, 'class'=>'avatar')); ?>
                            </div>
                            <div class="comment__content">
                                <div class="comment__info">
                                    <cite>Guest</cite>
                                    <div class="comment__meta">
                                        <time class="comment__time"><?= $this->Time->format('F jS, Y h:i A', $comment['Comment']['created_at'])?></time>
                                    </div>
                                </div>
                                <div class="comment__text">
                                <p><?= h($comment['Comment']['body']); ?></p>
                                </div>
                            </div>
                        </li>
        <?php       endif;
                endif; 
        endforeach; ?>
        </ol>
        <div class="respond">
            <h3 class="h2">Add Comment</h3>
            <?php if (!$authUser) : ?>
            <fieldset>
                <?php echo $this->Form->create('Comment'); ?>
                <div class="form-field">
                    <?php echo $this->Form->input('author', array('label'=>false, 'class'=>'full-width','placeholder'=>'Name')); ?>
                </div>
                <div class="form-field">
                    <?php echo $this->Form->input('email', array('label'=>false, 'class'=>'full-width', 'placeholder'=>'Email Address')); ?>
                </div>
                <div class="message form-field">
                    <?php echo $this->Form->input('body', array('label'=>false, 'class'=>'full-width', 'type'=>'text', 'placeholder'=>'Comment')); ?>
                </div>
                <?php echo $this->Form->input('post_id', array('type'=>'hidden', 'value' => $post['Post']['id'])); ?>
                <?php echo $this->Form->end('Comment', array('type'=>'submit', 'class'=>'submit btn--primary btn--large full-width')); ?>
            </fieldset>
            <?php else : ?>
            <fieldset>
                <?php echo $this->Form->create('Comment'); ?>
                <div class="message form-field">
                    <?php echo $this->Form->input('body', array('label'=>false, 'class'=>'full-width', 'type'=>'text', 'placeholder'=>'Comment')); ?>
                </div>
                <?php echo $this->Form->input('post_id', array('type'=>'hidden', 'value' => $post['Post']['id'])); ?>
                <?php echo $this->Form->end('Comment', array('type'=>'submit', 'class'=>'submit btn--primary btn--large full-width')); ?>
            </fieldset>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
