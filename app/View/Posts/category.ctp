<div class="s-content__header col-full">
    <h1 class="s-content__header-title"><?php echo __('Category in:' . $cat_name); ?></h1>  
</div>
<div class="row masonry-wrap">
    <div class="masonry">
        <div class="grid-sizer"></div>
        <?php if (empty($filtered)) :
            echo 'No posts has been published from this category yet'; 
        else :
            foreach ($filtered as $f) : ?>
                <article class="masonry__brick entry format-standard">
                    <?php if (!empty($f['Post']['post_pic'])) : ?>
                        <div class="entry__thumb">
                            <?=$this->Html->image(h($f['Post']['post_pic']), array('height'=>600, 'width'=>400, 'class'=>'entry__thumb-link'))?>
                        </div>
                    <?php endif; ?>
                    <div class="entry__text">
                        <div class="entry__header">
                            <div class="entry__date">
                                <a href="#"><?php $this->Time->format('F jS, Y h:i A', $f['Post']['created_at'])?></a>
                            </div>
                            <h1 class="entry__title"><?php echo $this->Html->link(h($f['Post']['title']), array('controller'=>'posts', 'action'=>'view', 'title' => Inflector::slug(utf8_encode(strtolower(h($f['Post']['title']))), '-'), 'id' => $f['Post']['id']))?></h1>
                        </div>
                        <div class="entry__excerpt">
                            <p><?= h($f['Post']['body'])?></p>
                        </div>
                    </div>
                </article>
            <?php endforeach; 
        endif; ?>
    </div>
</div>
