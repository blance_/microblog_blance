<?php echo $this->start('navigation'); ?>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-users')) . 'Users', array('controller'=>'users', 'action'=>'index'), array('escape' => false));?></li>
	<li class="active"><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')) . 'Posts', array('controller'=>'posts', 'action'=>'show'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-calendar')) . 'Timeline', array('controller'=>'posts', 'action'=>'timeline'), array('escape' => false));?></li>
	<li><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')) . 'Categories', array('controller'=>'categories', 'action'=>'categories'), array('escape' => false));?></li>
<?php $this->end(); ?>
<section class="content">
	<?= $this->Html->link('Add Post', array('controller'=>'posts','action' => 'add'), array('class'=>'btn btn-primary')) ?><br><br>
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><?php echo __('List of Posts'); ?></h3>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped" id="example1">
				<thead>
					<tr>
						<th>Id</th>
						<th>Post Id</th>
						<th>Title</th>
						<th>Body</th>
						<th>Author</th>
						<th>Views</th>
						<th>Picture</th>
						<th>Status</th>
						<th>Post Created</th>
						<th>Post Edited</th>
						<th>Reset Views</th>
						<th>Publish</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>	
				<tbody>
				<?php foreach($posts as $post) : ?>	
					<tr>
						<td><?= $post['Post']['id'];?></td>
						<td><?= $post['Post']['post_id']; ?></td>
						<td><?= $this->Html->link(h($post['Post']['title']), array('controller' => 'posts', 'action' => 'view', 'title' => Inflector::slug(utf8_encode(strtolower(h($post['Post']['title']))), '-'), 'id' => $post['Post']['id']), array('target' => '_blank')); ?></td>
						<td><?= h($post['Post']['body']);?></td>
						<td><?= h($post['User']['username']);?></td>
						<td><?= $post['Post']['views']; ?></td>
						<td><?php if (!empty($post['Post']['post_pic'])) : echo $this->Html->image(h($post['Post']['post_pic']), array('height'=>50, 'width'=>50)); else : echo '<i>No image</i>'; endif;?></td>
						<td><?php if ($post['Post']['status'] == 1) : echo 'Draft'; elseif ($post['Post']['status'] == 2) : echo 'Published'; else : echo 'Outdated'; endif;?></td>
						<td><?= $post['Post']['created_at']; ?></td>
						<td><?= $post['Post']['modified_at']; ?></td>
						<td><?= $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-undo')), array('controller'=>'posts', 'action'=>'reset', $post['Post']['id']), array('escape' => false, 'class'=>'btn btn-default'));?></td>
						<td><?php 						
							if ($post['Post']['status'] == 1) :
								echo $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('controller'=>'posts', 'action'=>'publish', $post['Post']['id']), array('escape' => false, 'class'=>'btn btn-default'));
							else :
								echo $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('controller'=>'posts', 'action'=>'unpublish', $post['Post']['id']), array('escape' => false, 'class'=>'btn btn-info'));
							endif; ?>
						</td>
						<td><?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')),array('controller'=>'posts', 'action'=>'edit', 'id' => $post['Post']['id']), array('escape' => false, 'class'=>'btn btn-default'));?>
						<td><?= $this->Form->postlink($this->Html->tag('i', '', array('class' => 'fa fa-trash')),array('controller'=>'posts', 'action'=>'delete', $post['Post']['id']), array('escape' => false, 'class'=>'btn btn-danger', 'confirm'=>'Are you sure you want to delete this post?'));?>
					</tr>
					<?php endforeach ?>
				</tbody>
				<?php unset($post); ?>
			</table>
		</div>
	</div>
</section>