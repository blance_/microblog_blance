<?php
App::uses('AppController', 'Controller');

class PostsController extends AppController {

	public $components = ['Session', 'Flash', 'Paginator'];
	public $helpers    = ['Html', 'Time', 'Paginator'];
	public $uses       = ['Post', 'Comment', 'User', 'Like', 'Category', 'PostsToCategory', 'Repost', 'Follower'];

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index', 'view', 'category', 'home');
	}
	
	/**
	 * This shows a list of posts
	 * @author blance
	 * @return void
	 */
	public function show() {
		$curr = $this->User->findById(AuthComponent::user('id'));
		if (!$curr['User']['role'] == 1) {
			throw new UnauthorizedException(__('Unauthorized access'));
		} else {
			$this->set('posts', $this->Post->find('all'));
		}
	}

	/**
	 * This view a specific post
	 * @author blance
	 * @return void
	 */
	public function view($title = null, $id = null) {
		$this->layout = 'user';
		$post         = $this->Post->findById($id);
		$user         = $this->User->findById(AuthComponent::user('id'));
		$authUser     = AuthComponent::user('id');
		$comments     = $this->Comment->find('all', ['conditions' => ['Comment.post_id' => $id]]);
		$liked        = $this->Like->field('id', ['Like.post_id' => $id, 'user_id' => AuthComponent::user('id')]);
		$com_count    = $this->Comment->find('count', ['conditions' => ['Comment.post_id' => $id, 'Comment.status'  => 1]]);
		$reposted     = $this->Post->field('id', ['Post.post_id' => $id, 'user_id' => AuthComponent::user('id')]);
		$tags         = $this->PostsToCategory->find('all', [
			'fields'  => [
				'Category.id',
				'Category.name',
			],
			'conditions' => [
				'PostsToCategory.post_id'    => $id,
				'PostsToCategory.category_id = Category.id'
			]
		]);
		$this->set(compact('post', 'comments', 'user', 'liked', 'com_count', 'authUser', 'reposted', 'tags'));
		if (!empty($post['Post']['title'])) {
			$this->set('title_for_layout', $post['Post']['title']);
			//This will increment the post views by 1 for every reload of the page
			if ($post['Post']['status'] == 2) {
				$count  = $this->Post->field('views', ['Post.id' => $id]);
				$count += 1;
			} else if($post['Post']['status'] == 1){
				throw new NotFoundException(__('Post still unpublished'));
			} 
		} else {
			throw new NotFoundException(__('Post not existing'));
		}
		$this->Post->updateAll(['Post.views' => $count], ['Post.id' => $id]);
		// This will send a comment from the page
		if ($this->request->is(['post'])) {
			$this->Comment->create();
			if ($this->Auth->login()) {
				$this->request->data['Comment']['user_id'] = AuthComponent::user('id');
				if (AuthComponent::user('id') == $post['Post']['user_id']) {
					$this->request->data['Comment']['status'] = 1;
					if($this->Comment->save($this->request->data)) {
						$this->redirect('/'. $title . '-' . $id);
					} 
				} 
			} else {
				if ($this->Comment->save($this->request->data)) {
					$this->Session->setFlash('Waiting for approval of your comment');
					$this->redirect('/'. $title . '-' . $id);
				}
			}
			$this->Flash->error(__('Unable to add your comment'));
		}
	}

	//This will add post to db
	public function add() {
		$curr = $this->User->findById(AuthComponent::user('id'));
		if (!$curr['User']['role'] == 1) {
			$this->layout = 'user';
		} 
		//This will add categories to the post
		if (!empty($this->request->data)) {
			if ($this->request->data['Category']['categories']) {
				$categories = explode(',', $this->request->data['Category']['categories']);
				foreach ($categories as $cat) {
					$cat = strtolower(trim($cat));
					if ($cat) {
						$this->Post->Category->recursive = -1;
						$category_name = $this->Post->Category->findByName($cat);
						if (!$category_name) {
							$this->Post->Category->create();
							$category_name = $this->Post->Category->save(['name' => $cat]);
							$category_name['Category']['id'] = $this->Post->Category->id;
							if (!$category_name) {
								$this->Session->setFlash(__(sprintf('The Category %s could not be saved', $cat), true));
							}
						}
						if ($category_name) {
							$this->request->data['Category']['Category'][$category_name['Category']['id']]  = $category_name['Category']['id'];
						}
					}
				}
			}
			$this->Post->hasAndBelongsToMany['Category']['unique'] = false;
			if ($this->request->is('post')) {
				$this->Post->create();
				if (!empty($this->request->data['Post']['upload']['name'])) {
					$file = $this->request->data['Post']['upload'];
					$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
					$arr_ext = ['jpg', 'jpeg', 'gif','png'];
					if (in_array($ext, $arr_ext)) {
						move_uploaded_file($file['tmp_name'], WWW_ROOT .'img/'. $file['name']);
						$this->request->data['Post']['post_pic'] = $file['name'];
					}
				}
				$this->request->data['Post']['user_id'] = AuthComponent::user('id');
				if ($this->Post->save($this->request->data)) {
					$this->Session->setFlash(__('The Post has been saved', true));
					$this->redirect('/create-post/');
				} else {
					$this->Session->setFlash(__('The Post could not be saved. Please try again.', true));
				}
			}	
		}
	}

	//This will edit a post
	public function edit($id = null) {
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		$post         = $this->Post->findById($id);
		if (!$post) {
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->request->is(['post', 'put'])) {
			$this->Post->id = $id;
			if (!empty($this->request->data['Post']['post_pic']['name'])) {
				$file    = $this->request->data['Post']['post_pic'];
				$ext     = substr(strtolower(strrchr($file['name'], '.')), 1);
				$arr_ext = ['jpg', 'jpeg', 'gif','png'];
				if (in_array($ext, $arr_ext)) {
					move_uploaded_file($file['tmp_name'], WWW_ROOT .'img/'. $file['name']);
					$this->request->data['Post']['post_pic'] = $file['name'];
				}
			}
			if ($this->request->data['Category']['categories']) {
				$this->PostsToCategory->deleteAll(['PostsToCategory.post_id' => $id]);
				$this->request->data['Post']['id'] = $id;
				$categories = explode(',', $this->request->data['Category']['categories']);
				foreach ($categories as $cat) {
					$cat = strtolower(trim($cat));
					if ($cat) {
						$this->Post->Category->recursive = -1;
						$category_name = $this->Post->Category->findByName($cat);
						if (!$category_name) {
							$this->Post->Category->create();
							$category_name = $this->Post->Category->save(['name' => $cat]);
							$category_name['Category']['id'] = $this->Post->Category->id;
							if (!$category_name) {
								$this->Session->setFlash(__(sprintf('The Category %s could not be saved', $cat), true));
							}
						}
						if ($category_name) {
							$this->request->data['Category']['Category'][$category_name['Category']['id']]  = $category_name['Category']['id'];
						}
					}
				}
			}
			$this->Post->hasAndBelongsToMany['Category']['unique'] = false;
			if  ($this->Post->save($this->request->data, false)) {
				$this->Flash->success(__('The post has been updated'));
				return $this->redirect(['action'=>'index']);
			}
			$this->Flash->error(__('Unable to update your post'));
		}
		if (!$this->request->data) {
			$this->request->data = $post;
		}
	}

	//This will edit comment for members only
	public function editComment($id = null) {
		$this->Comment->id = $id;
		$curr              = $this->User->findById(AuthComponent::user('id'));
		if (!$this->Comment->exists()) {
			throw new NotFoundException (__('Comment not existing'));
		}
		if (!$curr['User']['role'] == 1) {
			$this->layout = 'user';
		} 
		if ($this->request->is(['post', 'put'])) {
			$this->Comment->id = $id;
			if ($this->Comment->save($this->request->data)) {
				$this->Flash->success(__('The comment has been updated'));
				return $this->redirect(['action'=>'timeline']);
			}
			$this->Flash->error(__('Unable to update your comment'));
		}
		if (!$this->request->data) {
			$this->request->data = $comment;
		}
	}

	//This will delete post
	public function delete($id = null) {
		$curr = $this->User->findById(AuthComponent::user('id'));
		$this->request->allowMethod('post');
		if ($this->Post->delete($id)) {
			$this->Flash->success(__('The post has been deleted'));
			if (!$curr['User']['role'] == 1) {
				$this->redirect('/');
			} else {
				$this->redirect('/posts/');
			}
		} else {
			$this->Flash->error(__('The post could not been deleted'));
		}
	}

	//This will delete comment
	public function deleteComment($id) {
		$this->request->allowMethod('post');
		if ($this->Comment->delete($id)) {
			$this->Flash->success(__('The comment has been deleted'));
		} else {
			$this->Flash->success(__('The comment could not been deleted'));
		}
		return $this->redirect(['action' => 'timeline']);
	}

	//This will show posts on homepage
	public function index() {
		$this->layout = 'user';
		//If logged in, it will only show the posts by the user he/she follows, and retweets
		if ($this->Auth->loggedIn()) {
			$id = AuthComponent::user('id');
			$this->User->Behaviors->load('Containable');
			$options = [
				'joins' => [
					[
						'table' => 'posts',
						'alias' => 'Post',
						'type' => 'left',
						'foreignKey' => false,
						'conditions' => ['Post.user_id = User.id']
					],
					[
						'table' => 'followers',
						'alias' => 'Follower',
						'type' => 'left',
						'foreignKey' => false,
						'conditions' => ['Follower.user_id = Post.user_id']
					]
				],
				'conditions' => [
					'Post.id !=' => 0,
					'Follower.follower_id' => $id,
					'Post.status' => 2
				],
				'fields' => [
					'Post.id', 'Post.title', 'Post.user_id', 'Post.post_id', 'Post.post_pic', 'Post.created_at', 'Post.body', 'User.username', 'User.profile_pic'
				]
			];
			$posts = $this->User->find('all', $options);
			$this->set(compact('posts'));
		} else {
			$this->set('title_for_layout', 'MicroBlog');
			$this->Paginator->settings = ['conditions' => ['Post.status' => 2], 'limit' => 10];
			$posts = $this->Paginator->paginate('Post');
			$this->set(compact('posts'));
		}
	}

	//This will allow the admin to publish draft post and set status = 2
	public function publish($id = null) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		} else if ($this->request->is('post', 'put')) {
			if ($this->Post->updateAll(['Post.status' => 2], ['Post.id' => $id])) {
				$this->Flash->success(__('The post has been published'));
				return $this->redirect(['action'=>'show']);
			}
			$this->Flash->error(__('Unable to publish the post'));
		}
	}

	//This will allow the admin to unpublish published post and set status = 1
	public function unpublish($id = null) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		} else if ($this->request->is('post', 'put')) {
			if ($this->Post->updateAll(['Post.status' => 1], ['Post.id' => $id])) {
				$this->Flash->success(__('The post has been unpublished'));
				return $this->redirect(['action' => 'show']);
			}
			$this->Flash->error(__('Unable to unpublish the post'));
		}
	}

	//This will reset the number of views back to 0 that affects the post popularity
	public function reset($id = null) {
		$this->request->allowMethod(['post', 'put']);
		if ($this->request->is('post')) {
			$this->Post->updateAll(['Post.views' => 0], ['Post.id' => $id]);
			$this->Flash->successpost(__('Views has been reset'));
			return $this->redirect(['action' => 'show']);
		}
		$this->Flash->error(__('Unable to reset the views'));
	}

	//This will show the comments for each posts
	public function viewComments($id = null) {
		$this->layout = 'user';
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Post not existing'));
		}
		$comments     = $this->Comment->find('all', [
			'conditions' => [
				'Comment.post_id' => $id
			]
		]);
		$title        = $this->Post->field('title', ['Post.id' => $id]);
		$this->Paginator->settings = ['conditions' => ['Comment.post_id' => $id], 'limit' => 10];
		$comments     = $this->Paginator->paginate('Comment');
		$this->set(compact('comments', 'title', 'id'));
	}

	//This will allow the owner of the post to approve comments to be displayed in his/her post
	public function approve($id = null, $postid = null) {
		if ($this->request->is('post')) {
			$this->Comment->updateAll(['Comment.status' => 1], ['Comment.id' => $id]);
			$this->Flash->successpost(__('Comment status has been changed'));
			return $this->redirect('/view-comments/'. $postid);
		}
		$this->Flash->error(__('Unable to change the comment status'));
	}

	//This will allow the owner of the post to hide comments to be hidden in his/her post
	public function hide($id = null, $postid = null) {
		if ($this->request->is('post')) {
			$this->Comment->updateAll(['Comment.status' => 0], ['Comment.id' => $id]);
			$this->Flash->successpost(__('Comment status has been changed'));
			return $this->redirect('/view-comments/'. $postid);
		}
		$this->Flash->error(__('Unable to change the comment status'));
	}

	//This will show the posts under a specific category with a post status of 2
	public function category($id = null) {
		$this->layout = 'user';
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Category not existing'));
		}
		$this->set('cat_name', $this->Category->field('name', ['Category.id' => $id]));
		$filtered        = $this->PostsToCategory->find('all', [
			'fields'     => ['Category.id', 'Post.id', 'Post.title', 'Post.body', 'Post.post_pic', 'Post.created_at'],
			'conditions' => ['PostsToCategory.post_id = Post.id', 'Category.id' => $id, 'Post.status' => 2]
		]);
		$this->set('filtered', $filtered);
	}

	//This will allow the user to repost a post with a status of 2
	public function repost($postid = null) {
		$curr      = $this->User->findById(AuthComponent::user('id'));
		$post      = $this->Post->findById($postid);
        if ($this->request->is(['post'])) {
			$this->Post->create();
            $data = [
				'Post' => [
					'post_id' => $post['Post']['id'],
					'title' => $post['Post']['title'],
					'body' => $post['Post']['body'],
					'user_id' => AuthComponent::user('id'),
					'post_pic' => $post['Post']['post_pic'],
					'status' => $post['Post']['status'],
				],
			];
			$this->Post->save($data);
			$this->Session->setFlash(__('Reposted'));
			if (!$curr['User']['role'] == 1) {
				$this->redirect('/');
            } else {
                $this->redirect('/timeline/');
            }
        }
	}
	
	//This will allow the user to unrepost a post with a status of 2
	public function unrepost($postid) {
		$curr      = $this->User->findById(AuthComponent::user('id'));
		$repostid  = $this->Post->field('id', ['Post.post_id' => $postid]);
        if ($this->request->is(['post'])) {
            $this->Post->delete($repostid);
            $this->Session->setFlash(__('Unreposted'));
            if (!$curr['User']['role'] == 1) {
				$this->redirect('/');
            } else {
                $this->redirect('/timeline/');
            }
        }
	}

	//This will show the posts from the users for the admin
	public function timeline() {
		$id        = $this->Post->find('list', ['fields' => ['Post.id']]);
		$curr      = $this->User->findById(AuthComponent::user('id'));
		$posts     = $this->Post->find('all');

		$this->set(compact('liked', 'reposted', 'posts'));
		
		if (!$curr['User']['role'] == 1) {
			throw new UnauthorizedException(__('Unauthorized access'));
		} else {
			$comments  = $this->Comment->find('all', [
				'conditions' => [
					'Comment.post_id' => $id
				]
			]);
			$this->set(compact('posts'));

			if ($this->request->is('post')) {
				$this->Comment->create();
				$this->request->data['Comment']['user_id'] = AuthComponent::user('id');
				if($this->Comment->save($this->request->data)) {
					$this->Session->setFlash('The comment has been created');
					$this->redirect('timeline');
				}
				$this->Flash->error(__('Unable to add your comment'));
			}
		}
	}
}

