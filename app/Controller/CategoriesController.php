<?php
App::uses('AppController', 'Controller');

class CategoriesController extends AppController {

	public $uses = ['Category', 'PostsToCategory'];

	public function beforeFilter()  {
		parent::beforeFilter();
	}

	//This shows the frequency of the categories
	public function categories() {
		$this->Category->virtualFields['frequency'] = 0;
		$cat = $this->Category->find('all', [
			'fields' => [
				'Category.id', 'Category.name', 'COUNT(PostsToCategory.category_id) as Category__frequency',
			],
			'joins' => [
				[
				'table' => 'posts_to_categories',
				'alias' => 'PostsToCategory',
				'type'  => 'left',
				'conditions' => 'Category.id = PostsToCategory.category_id',
				'group' => 'PostsToCategory.category_id'
				]
			],
			'group' => [
				'PostsToCategory.category_id'
			],
		]);
		$this->set(compact('cat'));	
	}
}
