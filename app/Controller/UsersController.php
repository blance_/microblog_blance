<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {
	
	public $components = ['Session', 'Paginator'];
	public $helpers    = ['Html', 'Form', 'Js', 'Paginator'];
	public $uses       = ['User', 'Post', 'Like', 'Comment', 'Follower', 'PostsToCategory', 'Category'];

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('register', 'signin', 'verify', 'forgotPassword', 'resetPassword');
	}

	//This will show the lists of user
	public function index() {
		$curr = $this->User->findById(AuthComponent::user('id'));
		if (!$curr['User']['role'] == 1) {
			throw new UnauthorizedException(__('Unauthorized access'));
		} else {
			$title_for_layout = 'List of Users';
			$users            = $this->User->find('all');
			$this->set(compact('title_for_layout', 'users'));
		}
	}

	//This will allow to register a user from user UI
	public function newMember() {
		$curr = $this->User->findById(AuthComponent::user('id'));
		$role = ['1' => 'Admin', '2' => 'Member'];
		$this->set(compact('role'));
		if (!$curr['User']['role'] == 1) {
			throw new UnauthorizedException(__('Unauthorized access'));
		} else {
			$this->set('title_for_layout', 'Register');
			if ($this->request->is('post')) {
				$this->User->create();
				if ($this->User->save($this->request->data)) {
					$msg  = 'Click on the link below to complete registration' . '<br>';
					$msg .= 'http://cakeblog.com/users/verify/t:'. $hash . '/n:' . $this->data['User']['username'] . ' ';
					$Email = new CakeEmail('gmail');
					$Email 	-> from(['me@example.com' => 'MicroBlog CakePHP'])
							-> to($this->request->data['User']['email'])
							-> subject('Confirm your email address')
							-> emailFormat('html')
							-> send($msg);
					if ($Email) {
						$this->Session->setFlash('The user has been created, check your mail');
						$this->redirect('/login');
					}
				}
			}
		}
	}

	//This will allow to edit user profile
	public function edit($username = null) {
		$id = $this->User->field('id', ['User.username' => $username]);
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('User not exists'));
		}
		$curr = $this->User->findById(AuthComponent::user('id'));
		if (!$curr['User']['role'] == 1) {
			$this->layout = 'user';
			if ($this->request->is('post') || $this->request->is('put')) {
				if (!empty($this->request->data['User']['profile_pic']['name'])) {
					$file = $this->request->data['User']['profile_pic'];
					$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
					$arr_ext = ['jpg', 'jpeg', 'gif','png'];
					if (in_array($ext, $arr_ext)) {
						move_uploaded_file($file['tmp_name'], WWW_ROOT .'img/'. $file['name']);
						$this->request->data['User']['profile_pic'] = $file['name'];
					}
				}
				if ($this->User->save($this->request->data, false)) {
					$this->Session->setFlash('The user has been edited');
					return $this->redirect('/edit-profile/' . $username);
				}
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			} else {
				$this->request->data = $this->User->findById($id);
				unset($this->request->data['User']['password']);
			}
		} else {
			if ($this->request->is('post') || $this->request->is('put')) {
				if (!empty($this->request->data['User']['profile_pic']['name'])) {
					$file = $this->request->data['User']['profile_pic'];
					$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
					$arr_ext = ['jpg', 'jpeg', 'gif','png'];
					if (in_array($ext, $arr_ext)) {
						move_uploaded_file($file['tmp_name'], WWW_ROOT .'img/'. $file['name']);
						$this->request->data['User']['profile_pic'] = $file['name'];
					}
				}
				if ($this->User->save($this->request->data, false)) {
					$this->Session->setFlash('The user has been edited');
					return $this->redirect('/edit-profile/' . $username);
				}
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			} else {
				$this->request->data = $this->User->findById($id);
				unset($this->request->data['User']['password']);
			}
		}
    }

	//This will delete a user
	public function delete($id = null) {
		$this->request->allowMethod('post');
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException (__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash('The user has been deleted');
			$this->redirect('index');
		}
	}

	//This will allow the user to login
	public function login() {
		$this->set('title_for_layout', 'Login');
		$this->layout = 'user';
		if (!empty($this->data)) {
			if ($this->request->is('post')) {
				$user   = $this->User->findByUsername($this->request->data['User']['username']);
				$stat   = $user['User']['email_status'];
				$role   = $user['User']['role'];
				$active = $user['User']['active'];
				if ($this->Auth->login()) {
					if (!$stat == 0) {
						if (!$role == 1) {
							if ($active == 1) {
								$this->redirect('/');
							} else {
								$this->Session->setFlash(__('Your account is deactivated. Please contact your administrator'));
							}
						} else {
							$this->redirect('/admin');
						}
					} else {
						$this->Session->setFlash(__('Please activate your email first'));	
					}
				} else {
					$this->Session->setFlash(__('Invalid username or password'));
				}
			}
		}
	}

	//Thsi will allow the admin to login
	public function signin() {
		$this->set('title_for_layout', 'Login');
		$this->layout = 'login';
		if (!empty($this->data)) {
			if ($this->request->is('post')) {
				$user   = $this->User->findByUsername($this->request->data['User']['username']);
				$role   = $user['User']['role'];
				if ($this->Auth->login()) {
					if(!$role == 1) {
						throw new UnauthorizedException(__('Unauthorized access'));
					} else {
						$this->redirect('/users');
					}
				} else {
					$this->Session->setFlash(__('Invalid username or password'));
				}
			}
		}
	}

	//This will allow the admin to logout
	public function logout() {
		$curr = $this->User->findById(AuthComponent::user('id'));
		if (!$curr['User']['role'] == 1) {
			throw new UnauthorizedException(__('Unauthorized access'));
		} else {
			$this->Auth->logout();
			$this->redirect('/admin');
		}
	}

	//This will allow the user to logout
	public function userLogout() {
		$this->Auth->logout();
		$this->redirect('/');
	}

	//This will view the users profile from admin side
	public function profile($username = null) {
		$id   = $this->User->field('id', ['User.username' => $username]);
		$curr = $this->User->findById(AuthComponent::user('id'));
		if (!$curr['User']['role'] == 1) {
			throw new UnauthorizedException(__('Unauthorized access'));
		} else {
			$title_for_layout = 'Profile';
			$user             = $this->User->findById($id);
			$posts            = $this->Post->find('all', ['conditions'=>['Post.user_id'=>$id]]);
			$like             = $this->Like->find('all');
			$followed         = $this->Follower->field('id', ['user_id' => $id, 'follower_id' => AuthComponent::user('id')]);
			$liked            = $this->Like->field('id', ['post_id' => $id, 'user_id' => AuthComponent::user('id')]);
			$follower  		  = $this->Follower->find('count', ['conditions' => ['Follower.user_id' => $id]]);
			$following        = $this->Follower->find('count', ['conditions' => ['Follower.follower_id' => $id]]);
			
			$this->set(compact('title_for_layout','user', 'posts','like', 'followed', 'liked', 'follower', 'following'));

			if ($this->request->is('post')) {
				$this->Comment->create();
				$this->request->data['Comment']['user_id'] = AuthComponent::user('id');
				if($this->Comment->save($this->request->data)) {
					$this->Session->setFlash('The comment has been created', 'success');
					$this->redirect('/user-profile/' . $username);
				}
				$this->Flash->error(__('Unable to add your comment'));
			}
			
			if (!$user) {
				throw new NotFoundException(__('User not found'));
			}
		}
	}

	//This will view the users profile from the user side
	public function profileUser($username = null) {
		$id           = $this->User->field('id', ['User.username' => $username]);
		$this->layout = 'user';
		$this->Paginator->settings = ['conditions' => ['Post.status' => 2, 'Post.user_id' => $id], 'limit' => 10];
		$posts     = $this->Paginator->paginate('Post');
		$follower  = $this->Follower->find('count', ['conditions' => ['Follower.user_id' => $id]]);
		$following = $this->Follower->find('count', ['conditions' => ['Follower.follower_id' => $id]]);
		$followed  = $this->Follower->field('id', ['user_id' => $id, 'follower_id' => AuthComponent::user('id')]);
		$user      = $this->User->findById($id);

		if (!empty($this->request->data)) {
			if ($this->request->data['Category']['categories']) {
				$categories = explode(',', $this->request->data['Category']['categories']);
				foreach ($categories as $cat) {
					$cat = strtolower(trim($cat));
					if ($cat) {
						$this->Post->Category->recursive = -1;
						$cat_ = $this->Post->Category->findByName($cat);
						if (!$cat_) {
							$this->Post->Category->create();
							$cat_ = $this->Post->Category->save(['name' => $cat]);
							$cat_['Category']['id'] = $this->Post->Category->id;
							if (!$cat_) {
								$this->Session->setFlash(__(sprintf('The Category %s could not be saved', $cat), true));
							}
						}
						if ($cat_) {
							$this->request->data['Category']['Category'][$cat_['Category']['id']]  = $cat_['Category']['id'];
						}
					}
				}
			}
			$this->Post->hasAndBelongsToMany['Category']['unique'] = false;
			if ($this->request->is('post')) {
				$this->Post->create();
				if (!empty($this->request->data['Post']['upload']['name'])) {
					$file = $this->request->data['Post']['upload'];
					$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
					$arr_ext = ['jpg', 'jpeg', 'gif','png'];
					if (in_array($ext, $arr_ext)) {
						move_uploaded_file($file['tmp_name'], WWW_ROOT .'img/'. $file['name']);
						$this->request->data['Post']['post_pic'] = $file['name'];
					}
				}
				$this->request->data['Post']['user_id'] = AuthComponent::user('id');
				if ($this->Post->save($this->request->data)) {
					$this->Session->setFlash(__('The Post has been saved', true));
					$this->redirect('/create-post/');
				} else {
					$this->Session->setFlash(__('The Post could not be saved. Please try again.', true));
				}
			}	
		}
		if (!$user) {
			throw new NotFoundException(__('User not found'));
		}
		$this->set(compact('follower', 'following', 'posts', 'user', 'followed'));
	}

	//This will deactivate user account for non permanent deletion
	public function deactivate($id = null) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException(__('Method not allowed'));
		}
		if ($this->request->is('post')) {
			$this->User->updateAll(['User.active' => 0], ['User.id' => $id]);
			$this->Session->setFlash(__('Active status has been changed'));
			$this->redirect('/users/');
		}
		$this->Flash->error(__('Unable to deactivate this user'));
	}

	//This will activate user account
	public function activate($id = null) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException(__('Method not allowed'));
		}
		if ($this->request->is('post')) {
			$this->User->updateAll(['User.active' => 1], ['User.id' => $id]);
			$this->Session->setFlash(__('Active status has been changed'));
			$this->redirect('/users/');		
		}
		$this->Flash->error(__('Unable to activate this user'));
	}

	//This will allow to add a user from admin side
	public function register() {
		$this->set('title_for_layout', 'Register');
		$this->layout = 'user';
		if ($this->request->is('post')) {
			$this->User->create();
			$hash = sha1($this->data['User']['username']);
			$this->User->data['User']['activate_token'] = $hash;
			if ($this->User->save($this->request->data)) {
				$msg  = 'Click on the link below to complete registration' . '<br>';
				$msg .= 'http://cakeblog.com/users/verify/t:'. $hash . '/n:' . $this->data['User']['username'] . ' ';
				$Email = new CakeEmail('gmail');
				$Email 	-> from(['me@example.com' => 'MicroBlog CakePHP'])
						-> to($this->request->data['User']['email'])
						-> subject('Confirm your email address')
						-> emailFormat('html')
						-> send($msg);
				if ($Email) {
					$this->Session->setFlash('The user has been created, check your mail');
					$this->redirect('/login');
				}
			}
		}
	}

	//This will verify email address of the registered user
	public function verify() {
		if (!empty($this->passedArgs['n']) && !empty($this->passedArgs['t'])) {
			$name      = $this->passedArgs['n'];
			$tokenhash = $this->passedArgs['t'];
			$results   = $this->User->findByUsername($name);
			if ($results['User']['email_status'] == 0) {
				if ($results['User']['activate_token'] == $tokenhash) {
					$this->User->updateAll([
						'User.active' => 1, 
						'User.email_status' => 1
						]);
					$this->Follower->create();
					$data = [
						'Follower' => [
							'user_id' => $results['User']['id'],
							'follower_id' => $results['User']['id']
						],
					];
					$this->Follower->save($data);
					$this->Session->setFlash('Your registration is complete');
					$this->redirect('/login');
				} else {
					$this->Session->setFlash('Your registration failed please try again');
					$this->redirect('/register');
				}
			}  else {
				$this->Session->setFlash('Token has already been used');
				$this->redirect('/register');
			}
		}
	}

	//This will change password
	public function changePassword() {
		if ($this->request->data) {
			if (!empty($this->request->data['User']['password']) && !empty($this->request->data['User']['confirmpassword'])) {
				if ($this->request->data['User']['password'] == $this->request->data['User']['confirmpassword']) {
					$user = $this->User->findById(AuthComponent::user('id'));
					if (AuthComponent::password($this->request->data['User']['oldpassword']) == $user['User']['password']) {
						if ($this->User->updateAll([ 
							'User.password' => "'".AuthComponent::password($this->request->data['User']['password'])."'",
							'User.confirmpassword' => "'".AuthComponent::password($this->request->data['User']['confirmpassword'])."'"
						], ['User.id' => $user['User']['id']])) {
							$this->Session->setFlash('Password changed successfully');
							$this->redirect('/change-password/');
						}
					} else {
						$this->Session->setFlash('Current password does not match');
						$this->redirect('/change-password/');
					}
				} else {
					$this->Session->setFlash('Password and confirm password do not match');
					$this->redirect('/change-password/');
				}
			} else {
				$this->Session->setFlash('Password and confirm password cannot be empty');
				$this->redirect('/change-password/');
			}
		}
	}

	//This will send a code to change password
	public function forgotPassword() {
		$this->layout = 'login';
		$this->User->recursive = -1;
		if (!empty($this->request->data)) {
			if (empty($this->request->data['User']['email'])) {
				$this->Session->setFlash('Please provide your email address that you used to register');
			} else {
				$email = $this->request->data['User']['email'];
				$findEmail = $this->User->find('first', ['conditions' => ['User.email' => $email]]);
				if (!empty($findEmail)) {
					$key  = uniqid();
					$hash = md5($key);
					$url  = Router::url(['controller' => 'users', 'action' => 'resetPassword'], true) . '/' . $key . '#' . $hash;
					$ms   = $url;
					$ms   = wordwrap($ms, 1000);
					$findEmail['User']['password_token'] = $key;
					$this->User->id = $findEmail['User']['id'];
					if ($this->User->saveField('password_token', $findEmail['User']['password_token'])) {
						$Email  = new CakeEmail('gmail');
						$Email 	-> from(['me@example.com' => 'MicroBlog CakePHP'])
								-> to($findEmail['User']['email'])
								-> subject('Confirm your email address')
								-> emailFormat('html')
								-> send($ms);
						if ($Email->send()) {
							$this->Session->setFlash(__('Check your email to reset your password', true));
						} else {
							$this->Session->setFlash(__('Error generating reset link'));
						}
					}
				} else {
					$this->Session->setFlash('Email does not exist');
				}
			}
		}
	}

	//This will allow user to enter new password once the reset link has been clicked from user's email
	public function resetPassword($token = null) {
		$this->layout          = 'login';
		$this->User->recursive = -1;
		if (!empty($token)) {
			$user = $this->User->findByPasswordToken($token);
			if ($user) {
				$this->User->id = $user['User']['id'];
				if (!empty($this->request->data)) {
					$this->User->data = $this->request->data;
					$this->User->data['User']['email'] = $user['User']['email'];
					$new_hash = sha1($user['User']['email'] . rand(0, 100));
					$this->User->data['User']['password_token'] = $new_hash;
					if ($this->User->validates(['fieldList' => ['password', 'confirmpassword']])) {
						if ($this->User->updateAll([
							'User.password_token' => "'".$new_hash."'", 
							'User.password' => "'".AuthComponent::password($this->request->data['User']['password'])."'",
							'User.confirmpassword' => "'".AuthComponent::password($this->request->data['User']['confirmpassword'])."'"
						], ['User.id' => $user['User']['id']])) {
							$this->Session->setFlash('Password has been updated');
							$this->redirect(['controller' => 'users', 'action' => 'login']);
						}
					} else {
						$this->set('errors', $this->User->invalidFields());
					}
				}
			} else {
				$this->Session->setFlash('Token expired');
			}
		} else {
			$this->redirect('/');
		}
	}
}
