<?php
App::uses('Controller', 'Controller');

class AppController extends Controller {

	public $uses       = ['Post', 'Category', 'PostsToCategory'];
	public $components = ['Session', 'Auth', 'Paginator'];
	public $helpers    = ['Html', 'Form', 'Js', 'Paginator', 'Time'];

	//This shows the lists of categories and popular posts
	public function beforeFilter() {
		$popular     = $this->Post->find('all', [
			'order' => [
				'Post.views' => 'desc'
			], 
			'limit' => 6, 
			'conditions'=> [
				'Post.views !=' => 0
			]
		]);
        $categories  = $this->PostsToCategory->find('all', [
			'fields' => [
				'PostsToCategory.category_id', 'Category.name', 'Category.id', 'COUNT(category_id) as count'
			],
			'group'  => 'PostsToCategory.category_id',
			'order'  => 'count DESC',
			'limit'  => 10,
		]);
		$currentUser = $this->Session->read('Auth');
		$this->set(compact('popular', 'categories', 'currentUser'));
	}
}
