<?php 
App::uses('AppController', 'Controller');

class LikesController extends AppController {

    public $uses = ['Like'];

    public function beforeFilter() {
        parent::beforeFilter();
    }

    //This allows the user to like other persons/own posts
    public function like($postid, $userid) {
        $likerid = AuthComponent::user('id');
        if ($this->request->is(['post'])) {
            $this->Like->save(['post_id' => $postid, 'user_id' => $likerid]);
            $this->Session->setFlash(__('You liked this post'));
            $this->redirect(['controller' => 'posts', 'action' => 'timeline', $userid]);
        }
    }

    //This allows the user to unlike other persons/own posts
    public function unlike($id, $userid) {
        $likeid = $this->Like->field('id', ['post_id' => $id, 'user_id' => $userid]);
        if ($this->request->is(['post'])) {
            $this->Like->delete($likeid);
            $this->Session->setFlash(__('You unliked this post'));
            $this->redirect(['controller' => 'posts', 'action' => 'timeline', $userid]);
        }
    }
}