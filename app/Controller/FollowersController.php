<?php 
App::uses('AppController', 'Controller');

class FollowersController extends AppController {

    public $uses = ['Follower', 'User', 'Post'];

    public function beforeFilter() {
        parent::beforeFilter();
    }

    //This allows the user to follow other user except for himself/herself
    public function follow($username = null) {
        $userid     = $this->User->field('id', ['User.username' => $username]);
        $curr       = $this->User->findById(AuthComponent::user('id'));
        $followerid = AuthComponent::user('id');
        if ($this->request->is(['post'])) {
            if ($followerid == 1) {
                $this->Follower->save(['follower_id' => $followerid, 'user_id' => $userid]);
                $this->User->updateAll(['verified' => 1]);
                $this->Session->setFlash('Followed user', 'success');
                if ($curr['User']['role'] == 1) {
                    $this->redirect('/user-profile/' . $username);
                } else {
                    $this->redirect('/my-profile/' . $username);
                }
            } else {
                $this->Follower->save(['follower_id' => $followerid, 'user_id' => $userid]);
                $this->Session->setFlash('Followed user', 'success');
                if ($curr['User']['role'] == 1) {
                    $this->redirect('/user-profile/' . $username);
                } else {
                    $this->redirect('/my-profile/' . $username);
                }
            }
        } else {
            $this->Session->setFlash(__('Unable to follow'));
        }
    }

    //This allow the user to unfollow the other user except for himself/herself
    public function unfollow($username = null) {
        $userid     = $this->User->field('id', ['User.username' => $username]);
        $curr       = $this->User->findById(AuthComponent::user('id'));
        $followerid = $this->Follower->field('id', ['user_id' => $userid, 'follower_id' => AuthComponent::user('id')]);
        if ($this->request->is(['post'])) {
            $this->Follower->delete($followerid);
            $this->Session->setFlash('Unfollowed user',  'success');
            if ($curr['User']['role'] == 1) {
                $this->redirect('/user-profile/' . $username);
            } else {
                $this->redirect('/my-profile/' . $username);
            }
        }
    }

    //This shows the lists of the users' following
    public function showFollowing($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('User not existing'));
        }
        $title_for_layout = 'Following';
        $curr             = $this->User->findById(AuthComponent::user('id'));
        $username         = $this->User->field('username', ['User.id' => $id]);
		if (!$curr['User']['role'] == 1) {
            $this->layout = 'user';
			$options = [
                'joins' => [
                    [
                        'table'      => 'followers',
                        'alias'      => 'Follower',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'User.id = Follower.user_id'
                        ],
                    ],
                ],
                'conditions' => [
                    'Follower.follower_id' => $id,
                ]
            ];
            $following = $this->User->find('all', $options);
		} else {
			$options = [
				'joins' => [
					[
						'table'      => 'followers',
						'alias'      => 'Follower',
						'type'       => 'left',
						'conditions' => [
                            'User.id = Follower.user_id'
                        ]
                    ],
                ],
				'conditions' => [
					'Follower.follower_id' => $id,
                ],
				'fields' => [
					'User.username', 'User.firstname', 'User.lastname', 'User.profile_pic'
                ]
            ];
            $following = $this->User->find('all', $options);
        }
        $this->set(compact('username', 'following', 'title_for_layout'));
    }

    //This shows the lists of the users' followers
    public function showFollowers($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('User not existing'));
        }
        $title_for_layout = 'Followers';
        $curr             = $this->User->findById(AuthComponent::user('id'));
        $username     = $this->User->field('username', ['User.id' => $id]);
		if (!$curr['User']['role'] == 1) {
            $this->layout = 'user';
            $options = [
                'joins' => [
                    [
                        'table'      => 'followers',
                        'alias'      => 'Follower',
                        'type'       => 'left',
                        'conditions' => ['User.id = Follower.follower_id']
                    ],
                ],
                'conditions' => [
                    'Follower.user_id' => $id,
                ],
                'fields' => [
                    'User.username', 'User.firstname', 'User.lastname', 'User.profile_pic'
                ]
            ];
            $followers = $this->User->find('all', $options);
        } else {
            $options = [
                'joins' => [
                    [
                        'table'      => 'followers',
                        'alias'      => 'Follower',
                        'type'       => 'left',
                        'conditions' => ['User.id = Follower.follower_id']
                    ],
                ],
                'conditions' => [
                    'Follower.user_id' => $id,
                ],
                'fields' => [
                    'User.username', 'User.firstname', 'User.lastname', 'User.profile_pic'
                ]
            ];
            $followers = $this->User->find('all', $options);
        }
        $this->set(compact('username', 'followers', 'title_for_layout'));
    }
}
